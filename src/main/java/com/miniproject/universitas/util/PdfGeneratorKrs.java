package com.miniproject.universitas.util;

import java.awt.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;
import com.miniproject.universitas.dto.krs.KrsReport;
import com.miniproject.universitas.repository.NilaiRepository;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PdfGeneratorKrs {

    private final NilaiRepository nilaiRepository;

    public void generate(List<KrsReport> krsList, HttpServletResponse response) throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, response.getOutputStream());
        document.open();

        Font fontTitle = new Font(Font.TIMES_ROMAN, 30, Font.BOLD);
        Phrase title = new Phrase("UNIVERSITAS INDIVARA", fontTitle);
        Paragraph titleParagraph = new Paragraph(title);
        titleParagraph.setAlignment(Element.ALIGN_CENTER);
        document.add(titleParagraph);

        document.add(new Paragraph("\n"));
        document.add(new Chunk(new LineSeparator()));

        Font fontDaftarNilai = new Font(Font.TIMES_ROMAN, 20, Font.BOLD);
        Phrase daftarNilai = new Phrase("KARTU RENCANA STUDI", fontDaftarNilai);
        Paragraph daftarNilaiParagraph = new Paragraph(daftarNilai);
        daftarNilaiParagraph.setAlignment(Element.ALIGN_CENTER);
        document.add(daftarNilaiParagraph);
        document.add(new Paragraph("\n"));

        Font fontNamaNIM = new Font(Font.TIMES_ROMAN, 14, Font.NORMAL, new Color(0, 0, 0));
        document.add(new Paragraph("NAMA           : " + krsList.get(0).getNama(), fontNamaNIM));
        document.add(new Paragraph("NIM               : " + krsList.get(0).getNim(), fontNamaNIM));
        document.add(new Paragraph("JURUSAN     : " + krsList.get(0).getNamaJurusan(), fontNamaNIM));
        document.add(new Paragraph("FAKULTAS  : " + krsList.get(0).getNamaFakultas(), fontNamaNIM));
        document.add(new Paragraph("SEMESTER   : " + krsList.get(0).getSemester(), fontNamaNIM));
        document.add(new Paragraph("\n"));

        PdfPTable table = new PdfPTable(4); // Sesuaikan jumlah kolom dengan jumlah data
        table.setWidthPercentage(100);

        // Add table headers
        Font fontHeader = new Font(Font.TIMES_ROMAN, 15, Font.NORMAL, new Color(43, 159, 220));

        addCell(table, "ID", Element.ALIGN_CENTER, fontHeader);
        addCell(table, "Mata Kuliah", Element.ALIGN_CENTER, fontHeader);
        addCell(table, "Sks", Element.ALIGN_CENTER, fontHeader);
        addCell(table, "NID", Element.ALIGN_CENTER, fontHeader);

        // Add data to the table
        Font fontData = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0, 0, 0));
        for (KrsReport krs : krsList) {
            addCell(table, String.valueOf(krs.getId()), Element.ALIGN_CENTER, fontData);
            addCell(table, krs.getNamaMataKuliah(), Element.ALIGN_LEFT, fontData);
            addCell(table, String.valueOf(krs.getSks()), Element.ALIGN_CENTER, fontData);
            addCell(table, String.valueOf(krs.getNid()), Element.ALIGN_CENTER, fontData);
        }

        document.add(table);

        // Add total SKS and other information
        document.add(new Paragraph("\n"));
        document.add(new Paragraph("Total SKS  : " + krsList.stream().mapToInt(KrsReport::getSks).sum()));

        document.add(new Paragraph("\n"));
        document.add(new Paragraph("\n"));

        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("id"));
        String formattedDate = dateFormat.format(currentDate);

        Font fontTanggal = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0, 0, 0));
        Paragraph tanggalParagraph = new Paragraph("Jakarta, " + formattedDate, fontTanggal);
        tanggalParagraph.setAlignment(Element.ALIGN_RIGHT);
        document.add(tanggalParagraph);

        Font fontTandaTangan = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0, 0, 0));
        Paragraph tandaTanganParagraph = new Paragraph("Tanda Tangan", fontTandaTangan);
        tandaTanganParagraph.setAlignment(Element.ALIGN_RIGHT);
        document.add(tandaTanganParagraph);
        document.add(new Paragraph("\n"));

        Font fontNamaTtd = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0, 0, 0));
        Paragraph ttdnamaParagraph = new Paragraph("BAPAK CEO INDVARA", fontNamaTtd);
        ttdnamaParagraph.setAlignment(Element.ALIGN_RIGHT);
        document.add(ttdnamaParagraph);

        document.close();
    }

    private static void addCell(PdfPTable table, String text, int alignment, Font font) {
        PdfPCell cell = new PdfPCell(new Phrase(text, font));
        cell.setHorizontalAlignment(alignment);
        cell.setBackgroundColor(new Color(255, 255, 255)); // Set the background color for the header
        cell.setPadding(5);
        table.addCell(cell);
    }

    }
