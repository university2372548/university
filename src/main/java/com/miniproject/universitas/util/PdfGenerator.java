package com.miniproject.universitas.util;

import java.awt.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;
import com.miniproject.universitas.dto.nilai.NilaiReport;
import com.miniproject.universitas.repository.NilaiRepository;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import java.math.RoundingMode;

@Component
@RequiredArgsConstructor
public class PdfGenerator {
    private final NilaiRepository nilaiRepository;

public void generate(List<NilaiReport> nilaiList, HttpServletResponse response) throws DocumentException, IOException {
    Document document = new Document();
    PdfWriter.getInstance(document, response.getOutputStream());
    document.open();

    Font fontTitle = new Font(Font.TIMES_ROMAN, 30, Font.BOLD);
    Phrase title = new Phrase("UNIVERSITAS INDIVARA", fontTitle);
    Paragraph titleParagraph = new Paragraph(title);
    titleParagraph.setAlignment(Element.ALIGN_CENTER); // Menyusun teks ke tengah
    document.add(titleParagraph);

    document.add(new Paragraph("\n"));

// Tambahkan garis panjang setelah judul
    document.add(new Chunk(new LineSeparator()));


    Font fontDaftarNilai = new Font(Font.TIMES_ROMAN, 20,  Font.BOLD);
    Phrase daftarNilai = new Phrase("KARTU HASIL STUDI", fontDaftarNilai);
    Paragraph daftarNilaiParagraph = new Paragraph(daftarNilai);
    daftarNilaiParagraph.setAlignment(Element.ALIGN_CENTER); // Menyusun teks ke tengah
    document.add(daftarNilaiParagraph);
    document.add(new Paragraph("\n"));


    Font fontNamaNIM = new Font(Font.TIMES_ROMAN, 14, Font.NORMAL, new Color(0, 0, 0));
    document.add(new Paragraph("NAMA           : " + nilaiList.get(0).getNama(), fontNamaNIM));
    document.add(new Paragraph("NIM               : " + nilaiList.get(0).getNim(), fontNamaNIM));
    document.add(new Paragraph("JURUSAN     : " + nilaiList.get(0).getNamaJurusan(), fontNamaNIM));
    document.add(new Paragraph("FAKULTAS  : " + nilaiList.get(0).getNamaFakultas(), fontNamaNIM));
    document.add(new Paragraph("SEMESTER   : " + nilaiList.get(0).getSemester(), fontNamaNIM));
    document.add(new Paragraph("\n"));
    PdfPTable table = new PdfPTable(9);
    table.setWidthPercentage(100);
    table.setWidths(new float[]{3, 3, 3, 3, 3, 3, 3, 3, 3});

    PdfPCell cell = new PdfPCell();
    cell.setBackgroundColor(new Color(43, 159, 220));
    cell.setPadding(5);
    Font fontHeader = new Font(Font.TIMES_ROMAN, 15, Font.NORMAL, new Color(255, 255, 255));
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    cell.setPhrase(new Phrase("ID", fontHeader));
    table.addCell(cell);
    cell.setPhrase(new Phrase("Mata Kuliah", fontHeader));
    table.addCell(cell);
    cell.setPhrase(new Phrase("Sks", fontHeader));
    table.addCell(cell);
    cell.setPhrase(new Phrase("Kuis", fontHeader));
    table.addCell(cell);
    cell.setPhrase(new Phrase("Mid", fontHeader));
    table.addCell(cell);
    cell.setPhrase(new Phrase("Final", fontHeader));
    table.addCell(cell);
    cell.setPhrase(new Phrase("Nilai", fontHeader));
    table.addCell(cell);
    cell.setPhrase(new Phrase("Mutu", fontHeader));
    table.addCell(cell);
    cell.setPhrase(new Phrase("Grade", fontHeader));
    table.addCell(cell);
    int totalSks = 0;
    Integer totalMutu = 0;
    BigDecimal ipk = BigDecimal.ZERO;

    for (NilaiReport nilai : nilaiList) {
       table.addCell(String.valueOf(nilai.getIdKrs()));
        table.addCell(nilai.getNamaMataKuliah());
        table.addCell(String.valueOf(nilai.getSks()));
        table.addCell(String.valueOf(nilai.getKuis()));
        table.addCell(String.valueOf(nilai.getMidTest()));
        table.addCell(String.valueOf(nilai.getFinalTest()));
        table.addCell(String.valueOf(nilai.getNilaiTotal()));
        table.addCell(String.valueOf(nilai.getNilaiMutu()));
        table.addCell(nilai.getGrade());
        totalSks += nilai.getSks();
        totalMutu += nilai.getNilaiMutu();
        ipk = totalSks > 0 ? BigDecimal.valueOf(totalMutu).divide(BigDecimal.valueOf(totalSks), 2, RoundingMode.HALF_UP) : BigDecimal.ZERO;
    }
    document.add(new Paragraph("\n"));
    document.add(table);

    // Tambahkan total SKS dan total mutu di bawah tabel
    document.add(new Paragraph("\n"));
    document.add(new Paragraph("Total SKS  : " + totalSks));
    document.add(new Paragraph("Total Mutu : " + totalMutu));
    document.add(new Paragraph("IPK            : " + ipk));
    document.add(new Paragraph("\n"));
    document.add(new Paragraph("\n"));
    Date currentDate = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("id"));
    String formattedDate = dateFormat.format(currentDate);

    Font fontTanggal = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0, 0, 0)); // Warna hitam
    Paragraph tanggalParagraph = new Paragraph("Jakarta, " + formattedDate, fontTanggal);
    tanggalParagraph.setAlignment(Element.ALIGN_RIGHT); // Menyusun teks ke kanan
    document.add(tanggalParagraph);


    Font fontTandaTangan = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0, 0, 0)); // Warna hitam
    Paragraph tandaTanganParagraph = new Paragraph("Tanda Tangan", fontTandaTangan);
    tandaTanganParagraph.setAlignment(Element.ALIGN_RIGHT); // Menyusun teks ke kiri
    document.add(tandaTanganParagraph);
    document.add(new Paragraph("\n"));
    document.add(new Paragraph("\n"));
    Font fontNamaTtd = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0, 0, 0)); // Warna hitam
    Paragraph ttdnamaParagraph = new Paragraph("BAPAK CEO INDVARA", fontNamaTtd);
    ttdnamaParagraph.setAlignment(Element.ALIGN_RIGHT); // Menyusun teks ke kiri
    document.add(ttdnamaParagraph);
    document.close();
    document.close();


}}
