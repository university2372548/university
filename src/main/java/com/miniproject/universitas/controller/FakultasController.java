package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.fakultas.FakultasRequest;
import com.miniproject.universitas.dto.fakultas.FakultasResponse;
import com.miniproject.universitas.service.FakultasService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("universitas/fakultas")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class FakultasController {

    private final FakultasService fakultasService;

    @PostMapping
    public ResponseEntity<FakultasResponse> createFakultas(@RequestBody FakultasRequest fakultasRequest, @RequestHeader("token") String token) {
        FakultasResponse createFakultas = fakultasService.createFakultas(fakultasRequest, token);
        return new ResponseEntity<>(createFakultas, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<FakultasResponse>> getAllFakultas(@RequestHeader("token") String token) {
        List<FakultasResponse> fakultasList = fakultasService.getAllFakultas(token);
        return new ResponseEntity<>(fakultasList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FakultasResponse> getFakultasById(@PathVariable Integer id, @RequestHeader("token") String token) {
        FakultasResponse fakultas = fakultasService.getFakultasById(id, token);
        return new ResponseEntity<>(fakultas, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<FakultasResponse> updateFakultasById(@PathVariable Integer id, @RequestBody FakultasRequest fakultasRequest, @RequestHeader("token") String token) {
        FakultasResponse updatedFakultas = fakultasService.updateFakultasById(id, fakultasRequest, token);
        return new ResponseEntity<>(updatedFakultas, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFakultasById(@PathVariable Integer id, @RequestHeader("token") String token) {
        fakultasService.deleteFakultasById(id, token);
        return new ResponseEntity<>("Fakultas deleted successfully", HttpStatus.OK);
    }
}