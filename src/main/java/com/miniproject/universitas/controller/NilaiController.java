package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.nilai.NilaiReport;
import com.miniproject.universitas.dto.nilai.NilaiRequest;
import com.miniproject.universitas.dto.nilai.NilaiResponse;
import com.miniproject.universitas.service.NilaiService;
import com.miniproject.universitas.util.PdfGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import com.lowagie.text.DocumentException;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/universitas/nilai")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class NilaiController {

    private final NilaiService nilaiService;
    private final PdfGenerator pdfGenerator;

    @PostMapping
    public ResponseEntity<NilaiResponse> createNilai(@RequestBody NilaiRequest request, @RequestHeader("token") String token) {
        NilaiResponse response = nilaiService.createNilai(request, token);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    public ResponseEntity<List<NilaiReport>> getNilaiReport(@PathVariable("id") Integer id) {
        List<NilaiReport> response = nilaiService.getNilaiReportByIdMahasiswa(id);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/export-to-pdf/{id}")
    public void generatePdfFile(HttpServletResponse response, @PathVariable("id") Integer id) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String currentDateTime = dateFormat.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=student-" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        List<NilaiReport> listNilai = nilaiService.getNilaiReportByIdMahasiswa(id);

        pdfGenerator.generate(listNilai, response);
    }
    @PutMapping("/{id}")
    public ResponseEntity<NilaiResponse> updateNilai(@PathVariable("id") Integer id, @RequestBody NilaiRequest request, @RequestHeader("token") String token){
        NilaiResponse response = nilaiService.updateNilai(id, request, token);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable("id") Integer id){
        String response = nilaiService.deleteNilaiById(id);
        return ResponseEntity.ok(response);
    }
}
