package com.miniproject.universitas.controller;


import com.lowagie.text.DocumentException;
import com.miniproject.universitas.dto.krs.KrsDetailNilai;
import com.miniproject.universitas.dto.krs.KrsReport;
import com.miniproject.universitas.dto.krs.KrsRequest;
import com.miniproject.universitas.dto.krs.KrsResponse;
import com.miniproject.universitas.service.KrsService;
import com.miniproject.universitas.util.PdfGeneratorKrs;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("universitas/krs")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class KrsController {

    private final KrsService krsService;
    private final PdfGeneratorKrs pdfGeneratorKrs;

    @PostMapping
    public ResponseEntity<KrsResponse> createKrs(@RequestBody KrsRequest krsRequest, @RequestHeader("token") String token) {
        KrsResponse response = krsService.createKrs(krsRequest, token);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<KrsResponse> getKrsById(@PathVariable("id") Integer id){
        KrsResponse response =krsService.getKrsById(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<KrsDetailNilai>> getAllFakultas() {
        List<KrsDetailNilai> getlAllkrs = krsService.getAllKrs();
        return new ResponseEntity<>(getlAllkrs, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<KrsResponse> updateKrs(@PathVariable("id") Integer id, @RequestBody KrsRequest request, @RequestHeader("token") String token){
        KrsResponse response = krsService.updateKrs(id, request, token);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable("id") Integer id, @RequestHeader("token") String token){
        String response = krsService.deleteKrsById(id, token);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/nid/{nid}")
    public ResponseEntity<List<KrsDetailNilai>> getAllKrsByNid(@PathVariable("nid") String nid){
        List<KrsDetailNilai> response =krsService.getAllKrsByNid(nid);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/export-to-pdf/{id}")
    public void generatePdfFile(HttpServletResponse response, @PathVariable("id") Integer id)
            throws DocumentException, IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String currentDateTime = dateFormat.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=student-" + id + "-" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        List<KrsReport> listKrs = krsService.getKrsReportByIdMahasiswa(id);
        pdfGeneratorKrs.generate(listKrs, response);
    }
}
