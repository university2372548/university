package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.jurusan.JurusanRequest;
import com.miniproject.universitas.dto.jurusan.JurusanResponse;
import com.miniproject.universitas.service.JurusanService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/universitas/jurusan")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class JurusanController {

    private final JurusanService jurusanService;

    @PostMapping
    public ResponseEntity<JurusanResponse> createJurusan (@RequestBody JurusanRequest jurusanRequest, @RequestHeader("token") String token){
        JurusanResponse createJurusan = jurusanService.createJurusan(jurusanRequest, token);
        return new ResponseEntity<>(createJurusan, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<JurusanResponse>> getAllJurusan(@RequestHeader("token") String token){
        List<JurusanResponse> jurusanList = jurusanService.getAllJurusan(token);
        return new ResponseEntity<>(jurusanList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<JurusanResponse> getJurusanById(@PathVariable Integer id, @RequestHeader("token") String token){
        JurusanResponse jurusan = jurusanService.getJurusanById(id, token);
        return new ResponseEntity<>(jurusan, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<JurusanResponse> updateJurusanById(@PathVariable Integer id, @RequestBody JurusanRequest jurusanRequest, @RequestHeader("token") String token){
        JurusanResponse updatedJurusan = jurusanService.updateJurusanById(id, jurusanRequest, token);
        return new ResponseEntity<>(updatedJurusan, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteJurusanById(@PathVariable Integer id, @RequestHeader("token") String token){
        jurusanService.deleteJurusanById(id, token);
        return new ResponseEntity<>("Jurusan deleted successfully", HttpStatus.OK);
    }
}
