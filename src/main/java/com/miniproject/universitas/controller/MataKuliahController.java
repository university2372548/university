package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.matakuliah.MataKuliahRequest;
import com.miniproject.universitas.dto.matakuliah.MataKuliahResponse;
import com.miniproject.universitas.service.MataKuliahService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("universitas/matakuliah")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class MataKuliahController {

    private final MataKuliahService mataKuliahService;

    @PostMapping
    public ResponseEntity<MataKuliahResponse> createMataKuliah(@RequestBody MataKuliahRequest request, @RequestHeader("token") String token) {
    MataKuliahResponse response = mataKuliahService.createMataKuliah(request, token);
    return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<MataKuliahResponse>> getMataKuliahAll(){
        List<MataKuliahResponse> response = mataKuliahService.getMataKuliahAll();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{namaMataKuliah}")
    public ResponseEntity<List<MataKuliahResponse>> getMataKuliahByNamaMataKuliah(@PathVariable("namaMataKuliah") String namaMataKuliah, @RequestHeader("token") String token){
        List<MataKuliahResponse> response = mataKuliahService.getMataKuliahByNamaMataKuliah(namaMataKuliah, token);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MataKuliahResponse> updateMataKuliah(@PathVariable("id") Integer id, @RequestBody MataKuliahRequest request, @RequestHeader("token") String token){
        MataKuliahResponse response = mataKuliahService.updateMataKuliah(request, token, id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMataKuliah(@PathVariable("id") Integer id){
        mataKuliahService.deleteMataKuliah(id);
        return ResponseEntity.ok("Mata Kuliah deleted successfully");
    }
}
