package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.register.RegisterRequest;
import com.miniproject.universitas.dto.register.RegisterResponse;
import com.miniproject.universitas.service.RegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/Register")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor

public class RegisterController {

    private final RegisterService registerService;

    @PostMapping
    public ResponseEntity<RegisterResponse> registerUser(@RequestBody RegisterRequest request) {
        RegisterResponse response = registerService.registerUser(request);
        return ResponseEntity.ok(response);
    }
}
