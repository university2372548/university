package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.menu.MenuResponse;
import com.miniproject.universitas.service.MenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/menu")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class MenuController {

    private final MenuService menuService;


    @GetMapping("/{role}")
    public ResponseEntity<List<MenuResponse>> getAllMenu(@PathVariable("role") String role){
        List<MenuResponse> menuList = menuService.getAllMenu(role);
        return ResponseEntity.ok(menuList);
    }

}
