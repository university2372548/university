package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.dosen.DosenResponse;
import com.miniproject.universitas.service.DosenService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/universitas/dosen")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class DosenController {

    private final DosenService dosenService;


    @GetMapping("/{id}")
    public ResponseEntity<DosenResponse> getDosenById(@PathVariable Integer id, @RequestHeader("token") String token) {
        DosenResponse response = dosenService.getDosenById(id, token);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<DosenResponse>> getDosenAll(@RequestHeader("token") String token) {
        List<DosenResponse> response = dosenService.getDosenAll(token);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
