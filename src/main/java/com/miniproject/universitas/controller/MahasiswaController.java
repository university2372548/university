package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.mahasiswa.MahasiswaRequest;
import com.miniproject.universitas.dto.mahasiswa.MahasiswaResponse;
import com.miniproject.universitas.service.MahasiswaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/universitas/mahasiswa")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class MahasiswaController {


    private final MahasiswaService mahasiswaService;

    @PostMapping
    public ResponseEntity<MahasiswaResponse> createMahasiswa(@RequestBody MahasiswaRequest mahasiswaRequest, @RequestHeader("token") String token) {
        MahasiswaResponse response = mahasiswaService.createMahasiswa(mahasiswaRequest, token);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<MahasiswaResponse>> getAllMahasiswa(@RequestHeader("token") String token) {
        List<MahasiswaResponse> response = mahasiswaService.getMahasiswaAll(token);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MahasiswaResponse> getMahasiswaById(@PathVariable Integer id, @RequestHeader("token") String token) {
        MahasiswaResponse response = mahasiswaService.getMahasiswaById(id, token);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MahasiswaResponse> updateMahasiswa(@PathVariable Integer id, @RequestBody MahasiswaRequest mahasiswaRequest, @RequestHeader("token") String token) {
        MahasiswaResponse response = mahasiswaService.updateMahasiswa(id, mahasiswaRequest, token);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMahasiswa(@PathVariable Integer id, @RequestHeader("token") String token) {
        mahasiswaService.deleteMahasiswa(id, token);
        return ResponseEntity.ok("Mahasiswa deleted successfully");
    }
}