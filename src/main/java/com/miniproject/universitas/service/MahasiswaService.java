package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.mahasiswa.MahasiswaRequest;
import com.miniproject.universitas.dto.mahasiswa.MahasiswaResponse;
import com.miniproject.universitas.entity.Jurusan;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.repository.JurusanRepository;
import com.miniproject.universitas.repository.MahasiswaRepository;
import com.miniproject.universitas.validation.MahasiswaValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class MahasiswaService {

    private final MahasiswaRepository mahasiswaRepository;
    private final JurusanRepository jurusanRepository;
    private final MahasiswaValidation mahasiswaValidation;

    public MahasiswaResponse createMahasiswa (MahasiswaRequest mahasiswaRequest, String token) {
        mahasiswaValidation.validateUser(token);
        Mahasiswa mahasiswa = mahasiswaValidation.validateMahasiswaExists(mahasiswaRequest.getIdMahasiswa());
        mahasiswa.setId(mahasiswaRequest.getIdMahasiswa());
        Jurusan jurusan = jurusanRepository.findById(mahasiswaRequest.getIdJurusan()).orElse(null);
        mahasiswa.setJurusan(jurusan);
        mahasiswaRepository.save(mahasiswa);

        return mapMahasiswaToResponse(mahasiswa);
    }

    public List<MahasiswaResponse> getMahasiswaAll(String token){
        mahasiswaValidation.validateUser(token);
        return mahasiswaRepository.findAll().stream()
                .map(this::mapMahasiswaToResponse)
                .toList();
    }

    public MahasiswaResponse getMahasiswaById(Integer id, String token) {
        mahasiswaValidation.validateUserRole(token);
        return mapMahasiswaToResponse(mahasiswaValidation.validateMahasiswaExists(id));
    }

    public MahasiswaResponse updateMahasiswa(Integer id, MahasiswaRequest request, String token) {
        mahasiswaValidation.validateUser(token);
        Mahasiswa mahasiswa = mahasiswaValidation.validateMahasiswaExists(id);
        mahasiswa.setNama(request.getNama());
        Jurusan jurusan = jurusanRepository.findById(request.getIdJurusan()).orElse(null);
        mahasiswa.setJurusan(jurusan);
        mahasiswaRepository.save(mahasiswa);

        return mapMahasiswaToResponse(mahasiswa);
    }

    public String deleteMahasiswa(Integer id,String token) {
        mahasiswaValidation.validateUser(token);
        mahasiswaRepository.deleteById(id);
        return null;
    }

    public MahasiswaResponse mapMahasiswaToResponse(Mahasiswa mahasiswa) {
        MahasiswaResponse response = new MahasiswaResponse();
        response.setIdMahasiswa(mahasiswa.getId());
        response.setNim(mahasiswa.getNim());
        response.setNama(mahasiswa.getNama());
        if (mahasiswa.getJurusan() != null) {
            response.setNamaJurusan(mahasiswa.getJurusan().getNamaJurusan());
            if (mahasiswa.getJurusan().getFakultas() != null) {
                response.setNamaFakultas(mahasiswa.getJurusan().getFakultas().getNamaFakultas());
            } else {
                response.setNamaFakultas("Fakultas Belum di Daftarkan");
            }
        } else {
            response.setNamaJurusan("Jurusan Belum di Daftarkan");
            response.setNamaFakultas("Fakultas Belum di Daftarkan");
        }

        return response;
    }
}
