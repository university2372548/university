package com.miniproject.universitas.service;
import com.miniproject.universitas.dto.fakultas.FakultasRequest;
import com.miniproject.universitas.dto.fakultas.FakultasResponse;
import com.miniproject.universitas.entity.Fakultas;
import com.miniproject.universitas.repository.FakultasRepository;
import com.miniproject.universitas.validation.FakultasValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
@RequiredArgsConstructor
public class FakultasService {

    private final FakultasRepository fakultasRepository;
    private final FakultasValidation fakultasValidation;

    public FakultasResponse createFakultas(FakultasRequest fakultasRequest, String token) {
        fakultasValidation.validateUser(token);
        Fakultas fakultas = new Fakultas();
        fakultas.setNamaFakultas(fakultasRequest.getNamaFakultas());
        fakultasRepository.save(fakultas);
        return mapToResponse(fakultas);
    }

    public List<FakultasResponse> getAllFakultas(String token) {
        fakultasValidation.validateUser(token);
        return fakultasRepository.findAll().stream()
                .map(this::mapToResponse)
                .toList();
    }

    public FakultasResponse getFakultasById(Integer id, String token) {
        fakultasValidation.validateUser(token);
        Fakultas fakultas = fakultasValidation.validateAndGetFakultasById(fakultasRepository.findById(id), id);
        return mapToResponse(fakultas);
    }

    public FakultasResponse updateFakultasById(Integer id, FakultasRequest fakultasRequest, String token) {
        fakultasValidation.validateUser(token);
        Fakultas fakultas = fakultasValidation.validateAndGetFakultasById(fakultasRepository.findById(id), id);
        fakultas.setNamaFakultas(fakultasRequest.getNamaFakultas());
        fakultasRepository.save(fakultas);
        return mapToResponse(fakultas);
    }

    public String deleteFakultasById(Integer id, String token) {
        fakultasValidation.validateUser(token);
        fakultasRepository.deleteById(id);
        return null;
    }

    private FakultasResponse mapToResponse(Fakultas fakultas) {
        FakultasResponse response = new FakultasResponse();
        response.setIdFakultas(fakultas.getIdFakultas());
        response.setNamaFakultas(fakultas.getNamaFakultas());
        return response;
    }
}
