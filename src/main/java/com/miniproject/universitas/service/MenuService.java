package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.menu.MenuResponse;
import com.miniproject.universitas.entity.Menu;
import com.miniproject.universitas.repository.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MenuService {

    private final MenuRepository menuRepository;
    //Agar menu di dashboard terstruktur
    public List<MenuResponse> getAllMenu(String role) {
        List<MenuResponse> menuList = new ArrayList<>();
        for (Menu menu : menuRepository.findAll()) {
            if (menu.getRole().equalsIgnoreCase(role)) {
                menuList.add(MenuResponse.builder()
                        .id(menu.getId())
                        .namaMenu(menu.getNamaMenu())
                        .urlImage(menu.getUrlImage())
                        .role(menu.getRole())
                        .build());
            }
        }
        return menuList;
    }
}