package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.nilai.NilaiReport;
import com.miniproject.universitas.dto.nilai.NilaiRequest;
import com.miniproject.universitas.dto.nilai.NilaiResponse;
import com.miniproject.universitas.entity.Krs;
import com.miniproject.universitas.entity.Nilai;
import com.miniproject.universitas.repository.NilaiRepository;
import com.miniproject.universitas.validation.NilaiValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;


import java.math.RoundingMode;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NilaiService {

    private final NilaiRepository nilaiRepository;
    private final NilaiValidation nilaiValidation;

    public NilaiResponse createNilai (NilaiRequest request, String token){
        nilaiValidation.validateUserRole(token);
        Krs krs = nilaiValidation.validateAndGetKrs(request);
        Nilai nilai = createNilaiFromRequest(request, krs);
        nilaiRepository.save(nilai);

        return createNilaiResponse(nilai, krs.getMahasiswa().getNama());
    }

    private Nilai createNilaiFromRequest(NilaiRequest request, Krs krs) {
        Nilai nilai = new Nilai();
        nilai.setKrs(krs);
        nilai.setId(request.getIdKrs());
        nilai.setKuis(request.getKuis());
        nilai.setMidTest(request.getMidTest());
        nilai.setFinalTest(request.getFinalTest());
        BigDecimal total = request.getKuis().add(request.getMidTest()).add(request.getFinalTest()).divide(BigDecimal.valueOf(3), RoundingMode.HALF_UP);
        nilai.setNilaiTotal(total);
        nilai.setNilaiMutu(nilaiValidation.calculateNilaiMutu(nilai.getNilaiTotal(), krs.getMataKuliah().getSks()));
        nilai.setGrade(nilaiValidation.calculateGrade(nilai.getNilaiTotal()));
        return nilai;
    }

    public List<NilaiReport> getNilaiReportByIdMahasiswa(Integer id) {
        return nilaiRepository.findNilaiResponseByIdMahasiswa(id);
    }

    public NilaiResponse updateNilai(Integer id, NilaiRequest request, String token){
        nilaiValidation.validateUserRole(token);
        Nilai nilai = nilaiValidation.validateAndGetNilai(id);
        Krs krs = nilaiValidation.validateAndGetKrs(request);

        updateNilaiFromRequest(nilai, request, krs);
        nilaiRepository.save(nilai);

        return createNilaiResponse(nilai, krs.getMahasiswa().getNama());
    }
    private void updateNilaiFromRequest(Nilai nilai, NilaiRequest request, Krs krs) {
        nilai.setKrs(krs);
        nilai.setKuis(request.getKuis());
        nilai.setMidTest(request.getMidTest());
        nilai.setFinalTest(request.getFinalTest());
        BigDecimal total = request.getKuis().add(request.getMidTest()).add(request.getFinalTest()).divide(BigDecimal.valueOf(3), RoundingMode.HALF_UP);
        nilai.setNilaiTotal(total);
        nilai.setNilaiMutu(nilaiValidation.calculateNilaiMutu(nilai.getNilaiTotal(), krs.getMataKuliah().getSks()));
        nilai.setGrade(nilaiValidation.calculateGrade(nilai.getNilaiTotal()));
    }

    public String deleteNilaiById(Integer id){
        nilaiRepository.deleteById(id);
        return "Nilai Berhasil dihapus";
    }

    private NilaiResponse createNilaiResponse(Nilai nilai, String nama) {
        NilaiResponse response = new NilaiResponse();
        response.setNama(nama);
        response.setKuis(nilai.getKuis());
        response.setMidTest(nilai.getMidTest());
        response.setFinalTest(nilai.getFinalTest());
        response.setNilaiTotal(nilai.getNilaiTotal());
        response.setGrade(nilai.getGrade());
        response.setNilaiMutu(nilai.getNilaiMutu());
        return response;
    }
}
