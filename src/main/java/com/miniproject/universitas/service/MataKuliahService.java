package com.miniproject.universitas.service;
import com.miniproject.universitas.dto.matakuliah.MataKuliahRequest;
import com.miniproject.universitas.dto.matakuliah.MataKuliahResponse;
import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.entity.MataKuliah;
import com.miniproject.universitas.repository.MataKuliahRepository;
import com.miniproject.universitas.validation.MataKuliahValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class MataKuliahService {

    private final MataKuliahRepository mataKuliahRepository;
    private final MataKuliahValidation mataKuliahValidation;

    public MataKuliahResponse createMataKuliah(MataKuliahRequest request, String token){
        Dosen dosen = mataKuliahValidation.validateUserAndGetDosen(token, request.getIdDosen());

        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setNamaMataKuliah(request.getNamaMataKuliah());
        mataKuliah.setSks(request.getSks());
        mataKuliah.setDosen(dosen);
        mataKuliah = mataKuliahRepository.save(mataKuliah);

        return mapToResponse(mataKuliah);
    }

    public List<MataKuliahResponse> getMataKuliahAll(){
        return mataKuliahRepository.findAll().stream()
                .map(this::mapToResponse)
                .toList();
    }

    public MataKuliahResponse mapToResponse(MataKuliah mataKuliah) {
        MataKuliahResponse response = new MataKuliahResponse(mataKuliah.getId(), mataKuliah.getNamaMataKuliah(), mataKuliah.getSks(), mataKuliah.getDosen().getNama());
        response.setNamaMataKuliah(mataKuliah.getNamaMataKuliah());
        response.setSks(mataKuliah.getSks());
        response.setNamaDosenMataKuliah(mataKuliah.getDosen().getNama());
        return response;
    }

    public List<MataKuliahResponse> getMataKuliahByNamaMataKuliah(String namaMataKuliah, String token){
        mataKuliahValidation.validateAllUser(token);
        return mataKuliahRepository.findMataKuliahByNama(namaMataKuliah);
    }


    public MataKuliahResponse updateMataKuliah(MataKuliahRequest request, String token, Integer id){
        mataKuliahValidation.validateUserRole(token);
        MataKuliah mataKuliah = mataKuliahValidation.validateAndGetMataKuliahById(id);
        mataKuliah.setNamaMataKuliah(request.getNamaMataKuliah());
        mataKuliah.setSks(request.getSks());
        mataKuliahRepository.save(mataKuliah);

        return mapToResponse(mataKuliah);
    }

    public String deleteMataKuliah(Integer id){
        mataKuliahValidation.validateAndGetMataKuliahById(id);
        mataKuliahRepository.deleteById(id);
        return null;
    }
}
