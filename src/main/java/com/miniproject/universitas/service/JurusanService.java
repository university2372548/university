package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.jurusan.JurusanRequest;
import com.miniproject.universitas.dto.jurusan.JurusanResponse;
import com.miniproject.universitas.entity.Fakultas;
import com.miniproject.universitas.entity.Jurusan;
import com.miniproject.universitas.repository.JurusanRepository;
import com.miniproject.universitas.validation.JurusanValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
@RequiredArgsConstructor
public class JurusanService {

    private final JurusanRepository jurusanRepository;
    private final JurusanValidation jurusanValidation;

    public JurusanResponse createJurusan(JurusanRequest jurusanRequest, String token) {
        Fakultas fakultas = jurusanValidation.validateUserAndGetFakultas(token, jurusanRequest.getIdFakultas());
        Jurusan jurusan = new Jurusan();
        jurusan.setFakultas(fakultas);
        jurusan.setNamaJurusan(jurusanRequest.getNamaJurusan());
        jurusanRepository.save(jurusan);

        return mapToResponse(jurusan);
    }

    public List<JurusanResponse> getAllJurusan(String token) {
        jurusanValidation.validateUser(token);
        return jurusanRepository.findAll().stream()
                .map(this::mapToResponse)
                .toList();
    }

    public JurusanResponse getJurusanById(Integer id, String token) {
        jurusanValidation.validateUser(token);
        Jurusan jurusan = jurusanValidation.validateAndGetJurusanById(jurusanRepository.findById(id), id);
        return mapToResponse(jurusan);
    }

    public JurusanResponse updateJurusanById(Integer id, JurusanRequest jurusanRequest, String token) {
        jurusanValidation.validateUser(token);
        Jurusan jurusan = jurusanValidation.validateAndGetJurusanById(jurusanRepository.findById(id), id);
        jurusan.setNamaJurusan(jurusanRequest.getNamaJurusan());
        jurusanRepository.save(jurusan);
        return mapToResponse(jurusan);
    }

    public String deleteJurusanById(Integer id, String token) {
        jurusanValidation.validateUser(token);
        jurusanRepository.deleteById(id);
        return "Jurusan deleted successfully";
    }

    public JurusanResponse mapToResponse(Jurusan jurusan) {
        JurusanResponse response = new JurusanResponse();
        response.setIdJurusan(jurusan.getIdJurusan());
        response.setNamaJurusan(jurusan.getNamaJurusan());
        response.setNamaFakultas(jurusan.getFakultas().getNamaFakultas());
        return response;
    }
}

