package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.login.LoginRequest;
import com.miniproject.universitas.dto.login.LoginResponse;
import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.DosenRepository;
import com.miniproject.universitas.repository.MahasiswaRepository;
import com.miniproject.universitas.repository.UsersRepository;
import com.miniproject.universitas.util.JwtToken;
import com.miniproject.universitas.validation.UserValidation;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final UsersRepository usersRepository;
    private final DosenRepository dosenRepository;
    private final MahasiswaRepository mahasiswaRepository;
    private final UserValidation userValidation;

    public LoginResponse login(LoginRequest request){
        Users user = userValidation.validateUser(request.getNomorInduk(), request.getPassword());
        LoginResponse response = new LoginResponse();
        Users u = new Users();
        u.setId(user.getId());
        u.setPassword(request.getPassword());
        u.setEmail(user.getEmail());
        u.setRole(user.getRole());
        u.setAlamat(user.getAlamat());
        u.setNomorInduk(user.getNomorInduk());
        u.setId(user.getId());
        u.setToken(JwtToken.getToken(request));
        usersRepository.save(u);

        userValidation.validateUserRole(user);

        if (user.getRole().equalsIgnoreCase("dosen")) {
            Dosen dosen = dosenRepository.findByUsersId(user.getId()).orElseThrow(() -> new RuntimeException("Dosen not found"));

            response.setId(dosen.getId());
        } else {
            Mahasiswa mahasiswa = mahasiswaRepository.findByUsersId(user.getId()).orElseThrow(() -> new RuntimeException("Mahasiswa not found"));

            response.setId(mahasiswa.getId());

        }
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Login Success");
        response.setToken(u.getToken());
        response.setRole(u.getRole());
        response.setNomorInduk(u.getNomorInduk());
        return response;
    }
}
