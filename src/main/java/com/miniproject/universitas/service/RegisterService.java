package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.register.RegisterRequest;
import com.miniproject.universitas.dto.register.RegisterResponse;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.UsersRepository;
import com.miniproject.universitas.validation.RegisterValidation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


@Service
@Validated
@RequiredArgsConstructor
public class RegisterService {

    private final UsersRepository usersRepository;
    private final RegisterValidation registerValidation;

    public RegisterResponse registerUser(@Valid RegisterRequest request){
        Users users = new Users();
        RegisterResponse response = new RegisterResponse();

        String email = request.getEmail();
        registerValidation.validateEmail(email);

        users.setEmail(email);
        users.setAlamat(request.getAlamat());
        users.setPassword(request.getPassword());
        users.setRole(request.getRole());

        usersRepository.save(users);

        registerValidation.validateRoleAndCreateUser(request, users);

        response.setStatus(HttpStatus.OK.value());
        response.setMessage("Register Success");
        return response;
    }
}
