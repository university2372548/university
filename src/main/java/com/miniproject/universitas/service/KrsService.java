package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.krs.KrsDetailNilai;
import com.miniproject.universitas.dto.krs.KrsReport;
import com.miniproject.universitas.dto.krs.KrsRequest;
import com.miniproject.universitas.dto.krs.KrsResponse;
import com.miniproject.universitas.dto.menu.MenuResponse;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.entity.Menu;
import com.miniproject.universitas.repository.KrsRepository;
import com.miniproject.universitas.validation.KrsValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;



@Service
@RequiredArgsConstructor
public class KrsService {

    private final KrsRepository krsRepository;
    private final KrsValidation krsValidation;


    public KrsResponse createKrs(KrsRequest krsRequest, String token) {
        krsValidation.validateUser(token);
        Mahasiswa mahasiswa = krsValidation.validateKrsRequest(krsRequest);

        return krsValidation.validateAndCreateKrs(krsRequest, mahasiswa);
    }

    public KrsResponse getKrsById (Integer id){
        return krsValidation.getKrsDetails(id);
    }

    public List<KrsDetailNilai> getAllKrs(){
        return krsRepository.findAllKrs();
    }

    public KrsResponse updateKrs(Integer id, KrsRequest krsRequest, String token) {
        Mahasiswa mahasiswa = krsValidation.validateUserAndMahasiswa(token, krsRequest);

        return krsValidation.validateAndUpdateKrs(id, mahasiswa, krsRequest);
    }

    public String deleteKrsById(Integer id, String token){
        krsValidation.validateUserAccess(token);
        krsRepository.deleteById(id);
        return "KRS Berhasil dihapus";
    }

    public List<KrsReport> getKrsReportByIdMahasiswa(Integer id) {
        return krsRepository.findKrsDetailsByMahasiswaId(id);
    }

    public List<KrsDetailNilai> getAllKrsByNid(String nid) {
        List<KrsDetailNilai> krsDetailNilaiList = new ArrayList<>();
        for (KrsDetailNilai krsDetailNilai : krsRepository.findAllKrs()) {
            if (krsDetailNilai.getNid().equalsIgnoreCase(nid)) {
                krsDetailNilaiList.add(KrsDetailNilai.builder()
                        .id(krsDetailNilai.getId())
                        .nama(krsDetailNilai.getNama())
                        .nid(krsDetailNilai.getNid())
                        .namaMataKuliah(krsDetailNilai.getNamaMataKuliah())
                        .semester(krsDetailNilai.getSemester())
                        .build());
            }
        }
        return krsDetailNilaiList;
    }

}
