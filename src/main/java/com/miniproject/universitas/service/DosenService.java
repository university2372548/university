package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.dosen.DosenResponse;
import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.repository.DosenRepository;
import com.miniproject.universitas.validation.DosenValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DosenService {

    private final DosenRepository dosenRepository;
    private final DosenValidation dosenValidation;

    public DosenResponse getDosenById(Integer id, String token) {
        dosenValidation.validateUser(token);
        return mapDosenToResponse(dosenValidation.validateDosenExists(id));
    }

    public List<DosenResponse> getDosenAll(String token){
        dosenValidation.validateUser(token);
        return dosenRepository.findAll().stream()
                .map(this::mapDosenToResponse)
                .toList();
    }

    private DosenResponse mapDosenToResponse(Dosen dosen) {
        DosenResponse response = new DosenResponse();
        response.setIdDosen(dosen.getId());
        response.setNid(dosen.getNid());
        response.setNama(dosen.getNama());
        return response;
    }
}
