package com.miniproject.universitas.service;

import com.miniproject.universitas.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class TokenService {


    private final UsersRepository usersRepository;

    public boolean getToken(String token) {
        return usersRepository.findByToken(token).isPresent();
    }
}
