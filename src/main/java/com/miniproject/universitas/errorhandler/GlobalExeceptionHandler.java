package com.miniproject.universitas.errorhandler;

import com.miniproject.universitas.validation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class GlobalExeceptionHandler extends Exception {

    @ExceptionHandler(RegisterValidation.EmailValidationException.class)
    public ResponseEntity<com.miniproject.universitas.errorhandler.ErrorResponse> handleEmailValidationException(RegisterValidation.EmailValidationException e) {
        com.miniproject.universitas.errorhandler.ErrorResponse errorResponse = new ErrorResponse (e.getMessage(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }
    @ExceptionHandler(FakultasValidation.FakultasValidationException.class)
    public ResponseEntity<com.miniproject.universitas.errorhandler.ErrorResponse> handleFakultasValidationException(FakultasValidation.FakultasValidationException e) {
        com.miniproject.universitas.errorhandler.ErrorResponse errorResponse = new ErrorResponse(e.getMessage(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler(JurusanValidation.JurusanValidationException.class)
    public ResponseEntity<com.miniproject.universitas.errorhandler.ErrorResponse> handleJurusanValidationException(JurusanValidation.JurusanValidationException e) {
        com.miniproject.universitas.errorhandler.ErrorResponse errorResponse = new ErrorResponse(e.getMessage(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler(KrsValidation.KrsValidationException.class)
    public ResponseEntity<com.miniproject.universitas.errorhandler.ErrorResponse> handleKrsValidationException(KrsValidation.KrsValidationException e) {
        com.miniproject.universitas.errorhandler.ErrorResponse errorResponse = new ErrorResponse(e.getMessage(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler(NilaiValidation.NilaiValidationException.class)
    public ResponseEntity<com.miniproject.universitas.errorhandler.ErrorResponse> handleNilaiValidationException(NilaiValidation.NilaiValidationException e) {
        com.miniproject.universitas.errorhandler.ErrorResponse errorResponse = new ErrorResponse(e.getMessage(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler(UserValidation.UserValidationException.class)
    public ResponseEntity<com.miniproject.universitas.errorhandler.ErrorResponse> handleUserValidationException(UserValidation.UserValidationException e) {
        com.miniproject.universitas.errorhandler.ErrorResponse errorResponse = new ErrorResponse(e.getMessage(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler(DosenValidation.DosenValidationException.class)
    public ResponseEntity<com.miniproject.universitas.errorhandler.ErrorResponse> handleDosenValidationException(DosenValidation.DosenValidationException e) {
        com.miniproject.universitas.errorhandler.ErrorResponse errorResponse = new ErrorResponse(e.getMessage(), LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }
}
