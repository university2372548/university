package com.miniproject.universitas.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class Dosen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nid;
    private String nama;

    @ManyToOne
    private Users users;
}
