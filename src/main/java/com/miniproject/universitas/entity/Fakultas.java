package com.miniproject.universitas.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class Fakultas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idFakultas;
    private String namaFakultas;


}
