package com.miniproject.universitas.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class Krs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer semester;

    @ManyToOne
    @JoinColumn(name = "idMahasiswa")
    private Mahasiswa mahasiswa;

    @ManyToOne
    @JoinColumn(name = "idMataKuliah")
    private MataKuliah mataKuliah;
}
