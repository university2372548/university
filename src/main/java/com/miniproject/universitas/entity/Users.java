package com.miniproject.universitas.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String password;
    private String alamat;
    private String email;
    private String role;
    private String nomorInduk;

    @Column(length = 510)
    private String token;

}
