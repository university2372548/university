package com.miniproject.universitas.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;

@Entity
@Data
public class Nilai {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private Krs krs;

    private BigDecimal kuis;
    private BigDecimal midTest;
    private BigDecimal finalTest;
    private BigDecimal nilaiTotal;
    private Integer nilaiMutu;
    private String grade;

}
