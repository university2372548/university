package com.miniproject.universitas.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class Mahasiswa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nim;
    private String nama;

    @ManyToOne
    private Users users;

    @ManyToOne
    @JoinColumn(name = "idJurusan")
    private Jurusan jurusan;
}
