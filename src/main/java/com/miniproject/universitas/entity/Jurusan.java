package com.miniproject.universitas.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Jurusan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer idJurusan;
    private String namaJurusan;

    @ManyToOne
    @JoinColumn(name = "idFakultas")
    private Fakultas fakultas;

}
