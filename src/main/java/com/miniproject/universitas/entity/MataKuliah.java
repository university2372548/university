package com.miniproject.universitas.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class MataKuliah {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer id;
    private String namaMataKuliah;
    private Integer sks;

    @ManyToOne
    @JoinColumn(name = "idDosen")
    private Dosen dosen;

}
