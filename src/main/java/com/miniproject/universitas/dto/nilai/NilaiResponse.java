package com.miniproject.universitas.dto.nilai;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class NilaiResponse {
    private String nama;
    private BigDecimal kuis;
    private BigDecimal midTest;
    private BigDecimal finalTest;
    private BigDecimal nilaiTotal;
    private Integer nilaiMutu;
    private String grade;


}
