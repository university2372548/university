package com.miniproject.universitas.dto.nilai;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class NilaiRequest {
    private Integer idKrs;
    private BigDecimal kuis;
    private BigDecimal midTest;
    private BigDecimal finalTest;

}
