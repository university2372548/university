package com.miniproject.universitas.dto.nilai;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class NilaiReport {
    private Integer idKrs;
    private Integer semester;
    private String nim;
    private String nama;
    private String namaJurusan;
    private String namaFakultas;
    private String namaMataKuliah;
    private Integer sks;
    private BigDecimal kuis;
    private BigDecimal midTest;
    private BigDecimal finalTest;
    private BigDecimal nilaiTotal;
    private Integer nilaiMutu;
    private String grade;

}

