package com.miniproject.universitas.dto.menu;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class MenuResponse {

    private Integer id;
    private String namaMenu;
    private String urlImage;
    private String role;
}
