package com.miniproject.universitas.dto.matakuliah;


import lombok.Data;

@Data
public class MataKuliahRequest {
        private String namaMataKuliah;
        private Integer sks;
        private Integer idDosen;
}
