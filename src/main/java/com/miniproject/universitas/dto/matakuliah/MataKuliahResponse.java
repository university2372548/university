package com.miniproject.universitas.dto.matakuliah;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MataKuliahResponse {


    private Integer id;
    private String namaMataKuliah;
    private Integer sks;
    private String namaDosenMataKuliah;

}
