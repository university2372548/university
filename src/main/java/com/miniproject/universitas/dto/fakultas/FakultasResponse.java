package com.miniproject.universitas.dto.fakultas;

import lombok.Data;

@Data
public class FakultasResponse {

    private Integer idFakultas;
    private String namaFakultas;
}
