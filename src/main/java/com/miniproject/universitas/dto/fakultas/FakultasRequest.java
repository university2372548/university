package com.miniproject.universitas.dto.fakultas;


import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class FakultasRequest {

    @NotBlank(message = "Nama Fakultas tidak boleh kosong")
    private String namaFakultas;
}
