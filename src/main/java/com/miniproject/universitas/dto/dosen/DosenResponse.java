package com.miniproject.universitas.dto.dosen;

import lombok.Data;

@Data
public class DosenResponse {

    private String nid;
    private String nama;
    private Integer idDosen;

}
