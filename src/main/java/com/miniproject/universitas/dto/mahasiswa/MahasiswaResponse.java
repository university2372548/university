package com.miniproject.universitas.dto.mahasiswa;

import lombok.Data;

@Data
public class MahasiswaResponse {

    private Integer idMahasiswa;
    private String nim;
    private String nama;
    private String namaFakultas;
    private String namaJurusan;
}
