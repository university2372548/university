package com.miniproject.universitas.dto.mahasiswa;

import lombok.Data;

@Data
public class MahasiswaRequest {

    private Integer idMahasiswa;
    private Integer idJurusan;

    //Request Untuk Update
    private String nama;

}
