package com.miniproject.universitas.dto.jurusan;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class JurusanRequest {

    @NotBlank(message = "Nama Jurusan tidak boleh kosong")
    private Integer idFakultas;
    private String namaJurusan;
}
