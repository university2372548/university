package com.miniproject.universitas.dto.jurusan;

import lombok.Data;

@Data
public class JurusanResponse {

    private Integer idJurusan;
    private String namaJurusan;
    private String namaFakultas;
}
