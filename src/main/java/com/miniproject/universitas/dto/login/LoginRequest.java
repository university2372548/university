package com.miniproject.universitas.dto.login;

import lombok.Data;

@Data
public class LoginRequest {

    private String nomorInduk;
    private String password;

}
