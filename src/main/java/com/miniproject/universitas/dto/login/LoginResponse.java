package com.miniproject.universitas.dto.login;

import lombok.Data;

@Data
public class LoginResponse {
    private Integer id;
    private Integer status;
    private String message;
    private String token;
    private String role;
    private String nomorInduk;
}
