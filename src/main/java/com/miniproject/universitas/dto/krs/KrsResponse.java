package com.miniproject.universitas.dto.krs;

import lombok.Data;

import java.util.List;

@Data
public class KrsResponse {

    private String nama;
    private Integer semester;
    private List<String> mataKuliah;
    private List<Integer> sksList;
    private List<String> nid;
    private Integer totalSks;
}
