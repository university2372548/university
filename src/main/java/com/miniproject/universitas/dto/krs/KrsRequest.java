package com.miniproject.universitas.dto.krs;

import lombok.Data;

import java.util.List;

@Data
public class KrsRequest {

    private Integer id;
    private Integer semester;
    private List<Integer> idMataKuliah;
}
