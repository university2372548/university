package com.miniproject.universitas.dto.krs;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KrsReport {
    private Integer id;
    private String nama;
    private String nim;
    private String namaJurusan;
    private String namaFakultas;
    private Integer semester;
    private String namaMataKuliah;
    private Integer sks;
    private String nid;

}
