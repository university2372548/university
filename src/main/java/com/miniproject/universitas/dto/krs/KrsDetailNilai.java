package com.miniproject.universitas.dto.krs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class KrsDetailNilai {
    private Integer id;
    private String nama;
    private String nid;
    private String namaMataKuliah;
    private Integer semester;

}
