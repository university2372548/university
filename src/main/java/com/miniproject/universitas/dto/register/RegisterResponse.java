package com.miniproject.universitas.dto.register;

import lombok.Data;

@Data
public class RegisterResponse{

    private Integer status;
    private String message;
}
