package com.miniproject.universitas.repository;

import com.miniproject.universitas.entity.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MahasiswaRepository extends JpaRepository<Mahasiswa, Integer> {

   Optional<Mahasiswa> findById (Integer id);

   Optional<Mahasiswa> findByUsersId (Integer usersId);

}
