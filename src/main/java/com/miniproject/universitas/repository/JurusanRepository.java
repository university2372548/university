package com.miniproject.universitas.repository;

import com.miniproject.universitas.entity.Jurusan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JurusanRepository extends JpaRepository<Jurusan, Integer> {
}
