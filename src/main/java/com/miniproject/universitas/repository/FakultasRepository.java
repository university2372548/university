package com.miniproject.universitas.repository;

import com.miniproject.universitas.entity.Fakultas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FakultasRepository extends JpaRepository<Fakultas, Integer> {

}
