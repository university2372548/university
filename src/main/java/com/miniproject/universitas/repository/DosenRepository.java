package com.miniproject.universitas.repository;

import com.miniproject.universitas.entity.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DosenRepository extends JpaRepository <Dosen, Integer>{

    Dosen findByNid(String nid);

    Optional<Dosen> findByUsersId(Integer usersId);

}
