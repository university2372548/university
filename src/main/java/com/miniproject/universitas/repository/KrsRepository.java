package com.miniproject.universitas.repository;

import com.miniproject.universitas.dto.krs.KrsDetailNilai;
import com.miniproject.universitas.dto.krs.KrsReport;
import com.miniproject.universitas.entity.Krs;
import com.miniproject.universitas.entity.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KrsRepository extends JpaRepository<Krs, Integer> {

    @Query("SELECT k FROM Krs k WHERE k.mahasiswa.id = ?1")
    List<Krs> findByIdMahasiswa(Integer id);


    @Query("SELECT new com.miniproject.universitas.dto.krs.KrsReport(krs.id, mahasiswa.nama, mahasiswa.nim, jurusan.namaJurusan, fakultas.namaFakultas, krs.semester, mataKuliah.namaMataKuliah, mataKuliah.sks, dosen.nid) " +
            "FROM Krs krs " +
            "JOIN krs.mahasiswa mahasiswa " +
            "JOIN krs.mataKuliah mataKuliah " +
            "JOIN mataKuliah.dosen dosen " +
            "JOIN mahasiswa.jurusan jurusan " +
            "JOIN jurusan.fakultas fakultas " +
            "WHERE krs.mahasiswa.id = :id")
    List<KrsReport> findKrsDetailsByMahasiswaId(@Param("id") Integer id);

    boolean existsByMahasiswaAndSemester(Mahasiswa mahasiswa, Integer semester);

    @Query("SELECT new com.miniproject.universitas.dto.krs.KrsDetailNilai(krs.id, mahasiswa.nama, dosen.nid, matakuliah.namaMataKuliah, krs.semester) " +
            "FROM Krs krs " +
            "JOIN krs.mahasiswa mahasiswa join krs.mataKuliah matakuliah join matakuliah.dosen dosen")
    List<KrsDetailNilai> findAllKrs();

}
