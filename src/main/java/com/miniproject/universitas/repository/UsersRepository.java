package com.miniproject.universitas.repository;

import com.miniproject.universitas.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Integer> {


    Users findByNomorIndukAndPassword(String nomorInduk, String password);
    Optional<Users> findByToken(String token);
    Users findByEmail(String email);


}
