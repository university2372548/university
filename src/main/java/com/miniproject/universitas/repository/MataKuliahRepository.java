package com.miniproject.universitas.repository;
import com.miniproject.universitas.dto.matakuliah.MataKuliahResponse;
import com.miniproject.universitas.entity.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MataKuliahRepository extends JpaRepository<MataKuliah, Integer> {


    @Query("select new com.miniproject.universitas.dto.matakuliah.MataKuliahResponse(mk.id,  mk.namaMataKuliah, mk.sks, d.nama)" +
         " from MataKuliah mk join mk.dosen d where mk.namaMataKuliah=:namaMataKuliah")

    List<MataKuliahResponse> findMataKuliahByNama(@Param("namaMataKuliah") String namaMataKuliah);


}
