package com.miniproject.universitas.repository;

import com.miniproject.universitas.dto.nilai.NilaiReport;
import com.miniproject.universitas.entity.Nilai;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface NilaiRepository extends JpaRepository<Nilai, Integer> {

    @Query("select new com.miniproject.universitas.dto.nilai.NilaiReport(krs.id, krs.semester, mahasiswa.nim, mahasiswa.nama, jurusan.namaJurusan, fakultas.namaFakultas, matakuliah.namaMataKuliah, matakuliah.sks, nilai.kuis, nilai.midTest, nilai.finalTest, nilai.nilaiTotal, nilai.nilaiMutu, nilai.grade) " +
            "FROM Nilai nilai " +
            "JOIN nilai.krs krs " +
            "JOIN krs.mahasiswa mahasiswa " +
            "JOIN krs.mataKuliah matakuliah JOIN mahasiswa.jurusan jurusan " +
            "JOIN jurusan.fakultas fakultas " +
            "WHERE nilai.krs.mahasiswa.id=:id")
    List<NilaiReport> findNilaiResponseByIdMahasiswa(@Param("id") Integer id);
}

