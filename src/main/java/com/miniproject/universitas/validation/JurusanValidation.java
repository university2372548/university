package com.miniproject.universitas.validation;


import com.miniproject.universitas.entity.Fakultas;
import com.miniproject.universitas.entity.Jurusan;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.FakultasRepository;
import com.miniproject.universitas.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
@RequiredArgsConstructor
public class JurusanValidation {

    private final UsersRepository usersRepository;
    private final FakultasRepository fakultasRepository;

    public class JurusanValidationException extends Exception {
        public JurusanValidationException(String message) {
            super(message);
        }
    }

    @SneakyThrows
    public Fakultas validateUserAndGetFakultas(String token, Integer idFakultas) {
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new JurusanValidationException("User Tidak Ditemukan"));

        if (user.getRole().equalsIgnoreCase("mahasiswa")) {
            throw new JurusanValidationException("Hanya dosen yang dapat memiliki akses");
        }

        return fakultasRepository.findById(idFakultas)
                .orElseThrow(() -> new JurusanValidationException("Fakultas Tidak Ditemukan"));
    }

    @SneakyThrows
    public void validateUser(String token) {
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new JurusanValidationException("User Tidak Ditemukan"));

        if (user.getRole().equalsIgnoreCase("mahasiswa")) {
            throw new JurusanValidationException("Hanya dosen yang dapat memiliki akses");
        }
    }

    @SneakyThrows
    public Jurusan validateAndGetJurusanById(Optional<Jurusan> optionalJurusan, Integer id) {
        return optionalJurusan
                .orElseThrow(() -> new JurusanValidationException("Jurusan ID : " + id + " Tidak Ditemukan"));
    }
}
