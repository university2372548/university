package com.miniproject.universitas.validation;

import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.DosenRepository;
import com.miniproject.universitas.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DosenValidation {

    private final UsersRepository usersRepository;
    private final DosenRepository dosenRepository;

    public class DosenValidationException extends Exception {
        public DosenValidationException(String message) {
            super(message);
        }
    }


    @SneakyThrows
    public void validateUser(String token) {
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new DosenValidationException("User not found"));

        if (user.getRole().equalsIgnoreCase("mahasiswa")) {
            throw new DosenValidationException("Hanya DOSEN yang dapat memiliki akses ke MAHASISWA");
        }
    }

    @SneakyThrows
    public Dosen validateDosenExists(Integer id) {
        Dosen dosen = dosenRepository.findById(id).orElse(null);
        if (dosen == null) {
            throw new DosenValidationException("Dosen with id " + id + " not found");
        }
        return dosen;
    }

}
