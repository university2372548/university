package com.miniproject.universitas.validation;


import com.miniproject.universitas.dto.fakultas.FakultasResponse;
import com.miniproject.universitas.entity.Fakultas;

import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;



import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FakultasValidation {

    private final UsersRepository usersRepository;

    public class FakultasValidationException extends Exception {
        public FakultasValidationException(String message) {
            super(message);
        }
    }

    @SneakyThrows
    public void validateUser(String token){
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new FakultasValidationException("User not found"));

        if (user.getRole().equalsIgnoreCase("mahasiswa")) {
            throw new FakultasValidationException("Hanya dosen yang dapat memiliki akses");
        }
    }

    public FakultasResponse validateAndGetFakultas(Optional<Fakultas> optionalFakultas) {
        if (optionalFakultas.isPresent()) {
            Fakultas fakultas = optionalFakultas.get();

            FakultasResponse response = new FakultasResponse();
            response.setIdFakultas(fakultas.getIdFakultas());
            response.setNamaFakultas(fakultas.getNamaFakultas());

            return response;
        } else {
            return null;
        }
    }

    @SneakyThrows
    public Fakultas validateAndGetFakultasById(Optional<Fakultas> optionalFakultas, Integer id) {
        return optionalFakultas
                .orElseThrow(() -> new FakultasValidationException("Fakultas ID : " + id + " Tidak Ditemukan"));
    }
}
