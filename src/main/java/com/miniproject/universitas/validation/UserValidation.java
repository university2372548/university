package com.miniproject.universitas.validation;

import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.DosenRepository;
import com.miniproject.universitas.repository.MahasiswaRepository;
import com.miniproject.universitas.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserValidation {

    private final UsersRepository usersRepository;
    private final DosenRepository dosenRepository;
    private final MahasiswaRepository mahasiswaRepository;

    public class UserValidationException extends Exception {
        public UserValidationException(String message) {
            super(message);
        }
    }

    @SneakyThrows
    public Users validateUser(String nomorInduk, String password) {
        Users user = usersRepository.findByNomorIndukAndPassword(nomorInduk, password);
        if (user == null) {
            throw new UserValidationException("Nomor Induk Salah");
        }
        return user;
    }

    public void validateUserRole(Users user) {
        if (user.getRole().equalsIgnoreCase("dosen")) {
            Dosen dosen = dosenRepository.findByUsersId(user.getId()).orElse(null);
            assert dosen != null;
        }
        if (user.getRole().equalsIgnoreCase("mahasiswa")) {
            Mahasiswa mahasiswa = mahasiswaRepository.findByUsersId(user.getId()).orElse(null);
            assert mahasiswa != null;
        }
    }
}
