package com.miniproject.universitas.validation;

import com.miniproject.universitas.dto.nilai.NilaiRequest;
import com.miniproject.universitas.entity.Krs;
import com.miniproject.universitas.entity.Nilai;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.KrsRepository;
import com.miniproject.universitas.repository.NilaiRepository;
import com.miniproject.universitas.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


@Component
@RequiredArgsConstructor
public class NilaiValidation {

    private final UsersRepository usersRepository;
    private final KrsRepository krsRepository;
    private final NilaiRepository nilaiRepository;

    public class NilaiValidationException extends Exception {
        public NilaiValidationException(String message) {
            super(message);
        }
    }

    @SneakyThrows
    public void validateUserRole(String token) {
        Users user = usersRepository.findByToken(token)
                .orElseThrow (() -> new NilaiValidationException("User Tidak Ditemukan"));
        if (user.getRole().equalsIgnoreCase("mahasiswa")){
            throw new NilaiValidationException("Hanya Dosen yang dapat memiliki akses");
        }
    }

    @SneakyThrows
    public Krs validateAndGetKrs(NilaiRequest request) {
        return krsRepository.findById(request.getIdKrs())
                .orElseThrow(() -> new NilaiValidationException("KRS Tidak Ditemukan"));
    }

    public Integer calculateNilaiMutu(BigDecimal nilaiTotal, Integer sks) {
        if (nilaiTotal.compareTo(BigDecimal.valueOf(80)) >= 0) return sks * 4;
        if (nilaiTotal.compareTo(BigDecimal.valueOf(65)) >= 0) return sks * 3;
        if (nilaiTotal.compareTo(BigDecimal.valueOf(50)) >= 0) return sks * 2;
        if (nilaiTotal.compareTo(BigDecimal.valueOf(40)) >= 0) return sks * 1;
        return 0;
    }

    public String calculateGrade(BigDecimal nilaiTotal) {
        if (nilaiTotal.compareTo(BigDecimal.valueOf(80)) >= 0) return "A";
        if (nilaiTotal.compareTo(BigDecimal.valueOf(65)) >= 0) return "B";
        if (nilaiTotal.compareTo(BigDecimal.valueOf(50)) >= 0) return "C";
        if (nilaiTotal.compareTo(BigDecimal.valueOf(40)) >= 0) return "D";
        return "E";
    }

    @SneakyThrows
    public Nilai validateAndGetNilai(Integer id) {
        return nilaiRepository.findById(id)
                .orElseThrow (() -> new NilaiValidationException("Nilai Tidak Ditemukan"));
    }
}