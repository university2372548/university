package com.miniproject.universitas.validation;

import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.entity.MataKuliah;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.DosenRepository;
import com.miniproject.universitas.repository.MataKuliahRepository;
import com.miniproject.universitas.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class MataKuliahValidation {

    private final UsersRepository usersRepository;
    private final DosenRepository dosenRepository;
    private final MataKuliahRepository mataKuliahRepository;

    public Dosen validateUserAndGetDosen(String token, Integer idDosen) {
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new IllegalArgumentException("User not found"));

        if (user.getRole().equalsIgnoreCase("Mahasiswa")) {
            throw new IllegalArgumentException("Hanya dosen yang dapat memiliki akses");
        }

        return dosenRepository.findById(idDosen)
                .orElseThrow(() -> new IllegalArgumentException("Tidak ada dosen pada matakuliah tersebut"));
    }

    public void validateUserRole(String token) {
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new IllegalArgumentException("User Tidak Ditemukan"));

        if (user.getRole().equalsIgnoreCase("mahasiswa")) {
            throw new IllegalArgumentException("Hanya DOSEN yang dapat memiliki akses ke Mata Kuliah");
        }
    }
    public MataKuliah validateAndGetMataKuliahById(Integer id) {
        return mataKuliahRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("MataKuliah ID: " + id + " Tidak Ditemukan"));
    }

    public void validateAllUser(String token) {
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new IllegalArgumentException("User Tidak Ditemukan"));
        String role = user.getRole();

        if (!role.equals("mahasiswa") && !role.equals("dosen")) {
            throw new IllegalArgumentException("Access ditolak. Hanya Mahasiswa dan Dosen yang dapat mengakses");
        }
    }
}
