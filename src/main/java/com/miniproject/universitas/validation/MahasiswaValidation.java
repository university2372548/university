package com.miniproject.universitas.validation;


import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.MahasiswaRepository;
import com.miniproject.universitas.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class MahasiswaValidation {

    private final UsersRepository usersRepository;
    private final MahasiswaRepository mahasiswaRepository;

    public class MahasiswaValidationException extends Exception {
        public MahasiswaValidationException(String message) {
            super(message);
        }
    }

    @SneakyThrows
    public void validateUser(String token)  {
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new MahasiswaValidationException("User Tidak Ditemukan"));

        if (user.getRole().equalsIgnoreCase("mahasiswa")) {
            throw new MahasiswaValidationException("Hanya DOSEN yang dapat memiliki akses ke MAHASISWA");
        }
    }

    @SneakyThrows
    public void validateUserRole(String token) {
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new MahasiswaValidationException("User Tidak Ditemukan"));

        String role = user.getRole();
        if (!role.equalsIgnoreCase("mahasiswa") && !role.equalsIgnoreCase("dosen")) {
            throw new MahasiswaValidationException("Access ditolak. Hanya Mahasiswa dan Dosen yang dapat mengakses");
        }
    }

    @SneakyThrows
    public Mahasiswa validateMahasiswaExists(Integer id) {
        Mahasiswa mahasiswa = mahasiswaRepository.findById(id).orElse(null);
        if (mahasiswa == null) {
            throw new MahasiswaValidationException("Mahasiswa with id " + id + " not found");
        }
        return mahasiswa;
    }
}
