package com.miniproject.universitas.validation;

import com.miniproject.universitas.dto.register.RegisterRequest;
import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.DosenRepository;
import com.miniproject.universitas.repository.MahasiswaRepository;
import com.miniproject.universitas.repository.UsersRepository;
import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import java.util.Random;

@Component
@RequiredArgsConstructor
public class RegisterValidation {

    private final UsersRepository usersRepository;
    private final MahasiswaRepository mahasiswaRepository;
    private final DosenRepository dosenRepository;
    private final Random random = new Random();

    public class EmailValidationException extends Exception {
        public EmailValidationException(String message) {
            super(message);
        }
    }

    @SneakyThrows
    public void validateEmail(String email) {
        if (StringUtils.isBlank(email)) {
            throw new EmailValidationException("Email tidak boleh kosong.");
        }
        if (usersRepository.findByEmail(email) != null) {
            throw new EmailValidationException("Email already exists!");
        }
    }
    public Users validateRoleAndCreateUser(RegisterRequest request, Users users) {
        if (request.getRole().equalsIgnoreCase("Mahasiswa")) {
            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setUsers(usersRepository.findByEmail(request.getEmail()));
            mahasiswa.setNim("NIM" + String.format("%06d", random.nextInt(999999)));
            mahasiswa.setNama(request.getNama());
            mahasiswa.setUsers(users);
            mahasiswaRepository.save(mahasiswa);
            users.setNomorInduk(mahasiswa.getNim());
        } else if (request.getRole().equalsIgnoreCase("Dosen")) {
            Dosen dosen = new Dosen();
            dosen.setUsers(usersRepository.findByEmail(request.getEmail()));
            dosen.setNid("NID" + String.format("%06d", random.nextInt(999999)));
            dosen.setNama(request.getNama());
            dosen.setUsers(users);
            dosenRepository.save(dosen);
            users.setNomorInduk(dosen.getNid());
        }
        return usersRepository.save(users);
    }
}
