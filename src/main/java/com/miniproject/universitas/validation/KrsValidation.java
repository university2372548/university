package com.miniproject.universitas.validation;

import com.miniproject.universitas.dto.krs.KrsRequest;
import com.miniproject.universitas.dto.krs.KrsResponse;
import com.miniproject.universitas.entity.Krs;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.entity.MataKuliah;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.KrsRepository;
import com.miniproject.universitas.repository.MahasiswaRepository;
import com.miniproject.universitas.repository.MataKuliahRepository;
import com.miniproject.universitas.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class KrsValidation {

    private final UsersRepository usersRepository;
    private final MahasiswaRepository mahasiswaRepository;
    private final KrsRepository krsRepository;
    private final MataKuliahRepository mataKuliahRepository;

    public class KrsValidationException extends RuntimeException {
        public KrsValidationException(String message) {
            super(message);
        }
    }

    public Users validateUser(String token) {
        Users user = usersRepository.findByToken(token).orElseThrow(() -> new KrsValidationException("Token User Tidak Ditemukan"));
        if (user.getRole().equalsIgnoreCase("dosen")) {
            throw new KrsValidationException("Hanya Mahasiswa yang dapat memiliki akses");
        }
        return user;
    }

    public Mahasiswa validateKrsRequest(KrsRequest krsRequest) {
        Mahasiswa mahasiswa = mahasiswaRepository.findById(krsRequest.getId())
                .orElseThrow(() -> new KrsValidationException("Mahasiswa Tidak Ditemukan"));

        if (krsRepository.existsByMahasiswaAndSemester(mahasiswa, krsRequest.getSemester())) {
            throw new KrsValidationException("KRS pada semester tersebut sudah ada");
        }
        return mahasiswa;
    }

    public KrsResponse validateAndCreateKrs(KrsRequest krsRequest, Mahasiswa mahasiswa) {
        List<String> krsId = new ArrayList<>();
        List<String> nidList = new ArrayList<>();
        List<Integer> sksList = new ArrayList<>();
        int totalSks = 0;

        for (Integer idMatakuliah : krsRequest.getIdMataKuliah()) {
            MataKuliah mataKuliah = mataKuliahRepository.findById(idMatakuliah)
                    .orElseThrow(() -> new KrsValidationException("MataKuliah Tidak Ditemukan"));

            Krs krs = new Krs();
            krs.setMahasiswa(mahasiswa);
            krs.setSemester(krsRequest.getSemester());
            krs.setMataKuliah(mataKuliah);
            totalSks += mataKuliah.getSks();
            krsRepository.save(krs);

            krsId.add(mataKuliah.getNamaMataKuliah());
            sksList.add(mataKuliah.getSks());
            nidList.add(mataKuliah.getDosen().getNid());
        }

        KrsResponse krsResponse = new KrsResponse();
        krsResponse.setNama(mahasiswa.getNama());
        krsResponse.setSemester(krsRequest.getSemester());
        krsResponse.setMataKuliah(krsId);
        krsResponse.setNid(nidList);
        krsResponse.setSksList(sksList);
        krsResponse.setTotalSks(totalSks);
        return krsResponse;
    }

    public KrsResponse getKrsDetails(Integer id) {
        List<Krs> krsList = krsRepository.findByIdMahasiswa(id);
        List<String> mataKuliah = new ArrayList<>();
        List<String> nidList = new ArrayList<>();
        List<Integer> sksList = new ArrayList<>();
        int totalSks = 0;
        for (Krs krs : krsList) {
            mataKuliah.add(krs.getMataKuliah().getNamaMataKuliah());
            totalSks += krs.getMataKuliah().getSks();
            sksList.add(krs.getMataKuliah().getSks());
            nidList.add(krs.getMataKuliah().getDosen().getNid());
        }

        KrsResponse krsResponse = new KrsResponse();
        krsResponse.setNama(krsList.get(0).getMahasiswa().getNama());
        krsResponse.setSemester(krsList.get(0).getSemester());
        krsResponse.setMataKuliah(mataKuliah);
        krsResponse.setNid(nidList);
        krsResponse.setSksList(sksList);
        krsResponse.setTotalSks(totalSks);
        return krsResponse;
    }

    public Mahasiswa validateUserAndMahasiswa(String token, KrsRequest krsRequest) {
        Users user = usersRepository.findByToken(token).orElseThrow(() -> new IllegalArgumentException("Invalid Token Mahasiswa"));
        if (user.getRole().equalsIgnoreCase("dosen")) {
            throw new KrsValidationException("Hanya Mahasiswa yang dapat memiliki akses");
        }

        return mahasiswaRepository.findById(krsRequest.getId())
                .orElseThrow(() -> new KrsValidationException("Mahasiswa Tidak Ditemukan"));
    }

    public KrsResponse validateAndUpdateKrs(Integer id, Mahasiswa mahasiswa, KrsRequest krsRequest) {
        List<String> krsId = new ArrayList<>();
        List<String> nidList = new ArrayList<>();
        List<Integer> sksList = new ArrayList<>();
        int totalSks = 0;

        for (Integer idMatakuliah : krsRequest.getIdMataKuliah()) {
            MataKuliah mataKuliah = mataKuliahRepository.findById(idMatakuliah)
                    .orElseThrow(() -> new KrsValidationException("MataKuliah Tidak Ditemukan"));

            Krs krs = new Krs();
            krs.setId(id);
            krs.setMahasiswa(mahasiswa);
            krs.setSemester(krsRequest.getSemester());
            krs.setMataKuliah(mataKuliah);
            totalSks += mataKuliah.getSks();
            krsRepository.save(krs);

            krsId.add(mataKuliah.getNamaMataKuliah());
            sksList.add(mataKuliah.getSks());
            nidList.add(mataKuliah.getDosen().getNid());
        }

        KrsResponse krsResponse = new KrsResponse();
        krsResponse.setNama(mahasiswa.getNama());
        krsResponse.setSemester(krsRequest.getSemester());
        krsResponse.setMataKuliah(krsId);
        krsResponse.setNid(nidList);
        krsResponse.setSksList(sksList);
        krsResponse.setTotalSks(totalSks);
        return krsResponse;
    }

    public void validateUserAccess(String token) {
        Users user = usersRepository.findByToken(token)
                .orElseThrow(() -> new KrsValidationException("Invalid Token Dosen"));

        if (user.getRole().equalsIgnoreCase("mahasiswa")){
            throw new KrsValidationException("Hanya Dosen yang dapat memiliki akses");
        }
    }

}
