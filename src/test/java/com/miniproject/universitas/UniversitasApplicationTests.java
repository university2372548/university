package com.miniproject.universitas;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;

@SpringBootTest
class UniversitasApplicationTests {

	@Test
	void contextLoads() {
		UniversitasApplication mainApplication = new UniversitasApplication();
		assertNotNull(mainApplication);
	}

}
