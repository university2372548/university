package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.jurusan.JurusanRequest;
import com.miniproject.universitas.dto.jurusan.JurusanResponse;
import com.miniproject.universitas.entity.Fakultas;
import com.miniproject.universitas.entity.Jurusan;
import com.miniproject.universitas.repository.JurusanRepository;
import com.miniproject.universitas.validation.JurusanValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class JurusanServiceTest {

    @InjectMocks
    private JurusanService jurusanService;

    @Mock
    private JurusanRepository jurusanRepository;

    @Mock
    private JurusanValidation jurusanValidation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    //createJurusan
    @Test
    @DisplayName("Berhasil membuat jurusan")
    void createJurusanReturnsCorrectResponse() {
        JurusanRequest request = new JurusanRequest();
        request.setIdFakultas(1);
        request.setNamaJurusan("Test Jurusan");

        Fakultas fakultas = new Fakultas();
        fakultas.setNamaFakultas("Test Fakultas");

        Jurusan jurusan = new Jurusan();
        jurusan.setFakultas(fakultas);
        jurusan.setNamaJurusan(request.getNamaJurusan());

        when(jurusanValidation.validateUserAndGetFakultas(any(String.class), any(Integer.class))).thenReturn(fakultas);
        when(jurusanRepository.save(any(Jurusan.class))).thenReturn(jurusan);

        JurusanResponse response = jurusanService.createJurusan(request, "token");

        assertEquals(request.getNamaJurusan(), response.getNamaJurusan());
        assertEquals(fakultas.getNamaFakultas(), response.getNamaFakultas());
    }

    @Test
    @DisplayName("Gagal membuat jurusan karena fakultas tidak ditemukan")
    void createJurusanThrowsExceptionWhenFakultasNotFound() {
        JurusanRequest request = new JurusanRequest();
        request.setIdFakultas(1);
        request.setNamaJurusan("Test Jurusan");

        when(jurusanValidation.validateUserAndGetFakultas(any(String.class), any(Integer.class))).thenThrow(new RuntimeException("Fakultas not found"));

        assertThrows(RuntimeException.class, () -> jurusanService.createJurusan(request, "token"));
    }

    //getAllJurusan
    @Test
    @DisplayName("Berhasil mendapatkan semua jurusan")
    void getJurusanById_ValidIdAndToken_Success() {
        Integer id = 1;
        String token = "validToken";
        Jurusan jurusan = new Jurusan();
        Fakultas fakultas = new Fakultas();
        jurusan.setFakultas(fakultas); // Ensure that Fakultas is not null

        doNothing().when(jurusanValidation).validateUser(token);
        when(jurusanValidation.validateAndGetJurusanById(any(Optional.class), any(Integer.class))).thenReturn(jurusan);

        JurusanResponse response = jurusanService.getJurusanById(id, token);

        // Add null check before calling getNamaFakultas()
        if (jurusan.getFakultas() != null) {
            assertEquals(jurusan.getFakultas().getNamaFakultas(), response.getNamaFakultas());
        }
    }

    @Test
    @DisplayName("Tidak berhasil mendapatkan jurusan karena tidak ada jurusan")
    void getAllJurusanReturnsEmptyListWhenNoJurusanFound() {
        when(jurusanRepository.findAll()).thenReturn(new ArrayList<>());

        List<JurusanResponse> responseList = jurusanService.getAllJurusan("token");

        assertTrue(responseList.isEmpty());
    }

    //getJurusanById
    @Test
    @DisplayName("Berhasil mendapatkan jurusan berdasarkan id")
    void getJurusanById_ValidIdAndToken_Success2() {
        Integer id = 1; //Menambahkan inisialisasi id
        String token = "validToken"; //Menambahkan inisialisasi token
        Jurusan jurusan = new Jurusan(); //Menambahkan inisialisasi jurusan
        Fakultas fakultas = new Fakultas(); //Menambahkan inisialisasi fakultas
        jurusan.setFakultas(fakultas);

        doNothing().when(jurusanValidation).validateUser(token);
        when(jurusanValidation.validateAndGetJurusanById(any(Optional.class), any(Integer.class))).thenReturn(jurusan);

        JurusanResponse response = jurusanService.getJurusanById(id, token);

        // Menambahkan null check sebelum memanggil getNamaFakultas()
        if (jurusan.getFakultas() != null) {
            assertEquals(jurusan.getFakultas().getNamaFakultas(), response.getNamaFakultas());
        }
    }

    @Test
    @DisplayName("Tidak berhasil mendapatkan jurusan karena jurusan tidak ditemukan")
    void getJurusanByIdThrowsExceptionWhenJurusanNotFound() {
        when(jurusanValidation.validateAndGetJurusanById(any(Optional.class), any(Integer.class))).thenThrow(new RuntimeException("Jurusan not found"));

        assertThrows(RuntimeException.class, () -> jurusanService.getJurusanById(1, "token"));
    }

    //updateJurusanById
    @Test
    @DisplayName("Berhasil update jurusan berdasarkan id")
    void updateJurusanById_ValidRequestAndToken_Success() {
        Integer id = 1;
        String token = "validToken";
        JurusanRequest request = new JurusanRequest();
        request.setNamaJurusan("Updated Jurusan");

        Jurusan jurusan = new Jurusan();
        Fakultas fakultas = new Fakultas(); // Ensure that Fakultas is not null
        jurusan.setFakultas(fakultas);
        jurusan.setNamaJurusan("Test Jurusan");

        doNothing().when(jurusanValidation).validateUser(token);
        when(jurusanValidation.validateAndGetJurusanById(any(Optional.class), any(Integer.class))).thenReturn(jurusan);
        when(jurusanRepository.save(any(Jurusan.class))).thenReturn(jurusan);

        JurusanResponse response = jurusanService.updateJurusanById(id, request, token);

        // Add null check before calling getNamaFakultas()
        if (jurusan.getFakultas() != null) {
            assertEquals(request.getNamaJurusan(), response.getNamaJurusan());
        }
    }

    @Test
    @DisplayName("Tidak berhasil update jurusan karena jurusan tidak ditemukan")
    void updateJurusanByIdThrowsExceptionWhenJurusanNotFound() {
        JurusanRequest request = new JurusanRequest();
        request.setIdFakultas(1);
        request.setNamaJurusan("Updated Jurusan");

        when(jurusanValidation.validateAndGetJurusanById(any(Optional.class), any(Integer.class))).thenThrow(new RuntimeException("Jurusan not found"));

        assertThrows(RuntimeException.class, () -> jurusanService.updateJurusanById(1, request, "token"));
    }

    //deleteJurusanById
    @Test
    @DisplayName("Berhasil delete jurusan berdasarkan id")
    void deleteJurusanByIdReturnsCorrectResponse() {
        Integer id = 1;
        when(jurusanRepository.existsById(id)).thenReturn(true);
        String response = jurusanService.deleteJurusanById(id, "token");
        assertEquals("Jurusan deleted successfully", response);
    }

    @Test
    @DisplayName("Tidak berhasil delete jurusan karena jurusan tidak ditemukan karena token invalid")
    void deleteJurusanById_InvalidToken_ExceptionThrown() {
        Integer id = 1;
        String token = "invalidToken";

        doThrow(new IllegalArgumentException("Invalid token")).when(jurusanValidation).validateUser(token);

        assertThrows(IllegalArgumentException.class, () -> jurusanService.deleteJurusanById(id, token));
    }

    @Test
    @DisplayName("Tidak berhasil delete jurusan karena jurusan tidak ditemukan karena id tidak ditemukan")
    void deleteJurusanById_NonExistingId_ExceptionThrown() {
        Integer id = 1;
        String token = "validToken";

        doNothing().when(jurusanValidation).validateUser(token);
        doThrow(new EmptyResultDataAccessException(1)).when(jurusanRepository).deleteById(id);

        assertThrows(EmptyResultDataAccessException.class, () -> jurusanService.deleteJurusanById(id, token));
    }

}
