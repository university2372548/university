package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.matakuliah.MataKuliahRequest;
import com.miniproject.universitas.dto.matakuliah.MataKuliahResponse;
import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.entity.MataKuliah;
import com.miniproject.universitas.repository.MataKuliahRepository;
import com.miniproject.universitas.validation.MataKuliahValidation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class MataKuliahServiceTest {

    @InjectMocks
    private MataKuliahService mataKuliahService;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @Mock
    private MataKuliahValidation mataKuliahValidation;

    @BeforeEach
    void setUp() {
    }

    //Create Mata Kuliah
    @Test
    @DisplayName("Berhasil membuat mata kuliah")
    void createMataKuliah() {
        MataKuliahRequest request = new MataKuliahRequest();
        request.setNamaMataKuliah("Math");
        request.setSks(3);
        request.setIdDosen(1);

        Dosen dosen = new Dosen();
        dosen.setNama("Alvin");

        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setNamaMataKuliah(request.getNamaMataKuliah());
        mataKuliah.setSks(request.getSks());
        mataKuliah.setDosen(dosen);

        when(mataKuliahValidation.validateUserAndGetDosen(any(String.class), any(Integer.class))).thenReturn(dosen);
        when(mataKuliahRepository.save(any(MataKuliah.class))).thenReturn(mataKuliah);

        MataKuliahResponse response = mataKuliahService.createMataKuliah(request, "token");

        assertEquals(request.getNamaMataKuliah(), response.getNamaMataKuliah());
        assertEquals(request.getSks(), response.getSks());
        assertEquals(dosen.getNama(), response.getNamaDosenMataKuliah());
    }

    @Test
    @DisplayName("Gagal membuat mata kuliah karena token invalid")
    void createMataKuliah_InvalidToken() {
        MataKuliahRequest request = new MataKuliahRequest();
        request.setNamaMataKuliah("Math");
        request.setSks(3);
        request.setIdDosen(1);

        when(mataKuliahValidation.validateUserAndGetDosen(any(String.class), any(Integer.class))).thenThrow(new RuntimeException("Invalid token"));

        assertThrows(RuntimeException.class, () -> mataKuliahService.createMataKuliah(request, "invalid_token"));
    }

    @Test
    @DisplayName("Gagal membuat mata kuliah karena dosen id invalid")
    void createMataKuliah_InvalidDosenId() {
        MataKuliahRequest request = new MataKuliahRequest();
        request.setNamaMataKuliah("Math");
        request.setSks(3);
        request.setIdDosen(-1);

        when(mataKuliahValidation.validateUserAndGetDosen(any(String.class), any(Integer.class))).thenThrow(new RuntimeException("Invalid Dosen ID"));

        assertThrows(RuntimeException.class, () -> mataKuliahService.createMataKuliah(request, "token"));
    }

    //Get Mata Kuliah All
    @Test
    @DisplayName("Berhasil mendapatkan semua mata kuliah")
    void getMataKuliahAll_ReturnsAllMataKuliah() {
        Dosen dosen1 = new Dosen();
        dosen1.setNama("Dosen1");
        MataKuliah mataKuliah1 = new MataKuliah();
        mataKuliah1.setNamaMataKuliah("Math");
        mataKuliah1.setDosen(dosen1);

        Dosen dosen2 = new Dosen();
        dosen2.setNama("Dosen2");
        MataKuliah mataKuliah2 = new MataKuliah();
        mataKuliah2.setNamaMataKuliah("Physics");
        mataKuliah2.setDosen(dosen2);

        List<MataKuliah> mataKuliahList = List.of(mataKuliah1, mataKuliah2);
        when(mataKuliahRepository.findAll()).thenReturn(mataKuliahList);

        List<MataKuliahResponse> responseList = mataKuliahService.getMataKuliahAll();
        assertEquals(2, responseList.size());
        assertEquals("Math", responseList.get(0).getNamaMataKuliah());
        assertEquals("Physics", responseList.get(1).getNamaMataKuliah());
    }

    @Test
    @DisplayName("Gagal mendapatkan semua mata kuliah karena tidak ada mata kuliah")
    void getMataKuliahAll_ReturnsEmptyListWhenNoMataKuliah() {
        when(mataKuliahRepository.findAll()).thenReturn(Collections.emptyList());
        List<MataKuliahResponse> responseList = mataKuliahService.getMataKuliahAll();
        assertTrue(responseList.isEmpty());
    }

    //Get Mata Kuliah by Nama Mata Kuliah
    @Test
    @DisplayName("Berhasil mendapatkan mata kuliah berdasarkan nama mata kuliah")
    void getMataKuliahByNamaMataKuliah_Success() {
        String namaMataKuliah = "Math";
        Dosen dosen = new Dosen();
        dosen.setNama("Dosen1");
        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setNamaMataKuliah(namaMataKuliah);
        mataKuliah.setDosen(dosen);

        MataKuliahResponse mataKuliahResponse = mataKuliahService.mapToResponse(mataKuliah);

        when(mataKuliahRepository.findMataKuliahByNama(namaMataKuliah)).thenReturn(List.of(mataKuliahResponse));

        List<MataKuliahResponse> responseList = mataKuliahService.getMataKuliahByNamaMataKuliah(namaMataKuliah, "token");

        assertEquals(1, responseList.size());
        assertEquals(namaMataKuliah, responseList.get(0).getNamaMataKuliah());
    }

    @Test
    @DisplayName("Gagal mendapatkan mata kuliah berdasarkan nama mata kuliah karena nama mata kuliah tidak ditemukan")
    void getMataKuliahByNamaMataKuliah_NoMataKuliahFound() {
        String namaMataKuliah = "NonExistentName";

        when(mataKuliahRepository.findMataKuliahByNama(namaMataKuliah)).thenReturn(Collections.emptyList());

        List<MataKuliahResponse> responseList = mataKuliahService.getMataKuliahByNamaMataKuliah(namaMataKuliah, "token");

        assertTrue(responseList.isEmpty());
    }

    @Test
    @DisplayName("Gagal mendapatkan mata kuliah berdasarkan nama mata kuliah karena token invalid")
    void getMataKuliahByNamaMataKuliah_InvalidToken() {
        String namaMataKuliah = "Math";

        doThrow(new RuntimeException("Invalid token")).when(mataKuliahValidation).validateAllUser(any(String.class));

        assertThrows(RuntimeException.class, () -> mataKuliahService.getMataKuliahByNamaMataKuliah(namaMataKuliah, "invalid_token"));
    }

    @Test
    @DisplayName("Berhasil update mata kuliah")
    void updateMataKuliah_Success() {
        MataKuliahRequest request = new MataKuliahRequest();
        request.setNamaMataKuliah("UpdatedMath");
        request.setSks(4);
        request.setIdDosen(1);

        Dosen dosen = new Dosen();
        dosen.setNama("Dosen1");

        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setId(1);
        mataKuliah.setNamaMataKuliah("Math");
        mataKuliah.setSks(3);
        mataKuliah.setDosen(dosen);

        when(mataKuliahValidation.validateAndGetMataKuliahById(any(Integer.class))).thenReturn(mataKuliah);
        when(mataKuliahRepository.save(any(MataKuliah.class))).thenReturn(mataKuliah);

        MataKuliahResponse response = mataKuliahService.updateMataKuliah(request, "token", 1);

        assertEquals(request.getNamaMataKuliah(), response.getNamaMataKuliah());
        assertEquals(request.getSks(), response.getSks());
    }

    @Test
    @DisplayName("Gagal update mata kuliah karena token invalid")
    void updateMataKuliah_InvalidToken() {
        MataKuliahRequest request = new MataKuliahRequest();
        request.setNamaMataKuliah("UpdatedMath");
        request.setSks(4);
        request.setIdDosen(1);

        doThrow(new RuntimeException("Invalid token")).when(mataKuliahValidation).validateUserRole(any(String.class));

        assertThrows(RuntimeException.class, () -> mataKuliahService.updateMataKuliah(request, "invalid_token", 1));
    }

    @Test
    @DisplayName("Gagal update mata kuliah karena mata kuliah id invalid")
    void updateMataKuliah_InvalidMataKuliahId() {
        MataKuliahRequest request = new MataKuliahRequest();
        request.setNamaMataKuliah("UpdatedMath");
        request.setSks(4);
        request.setIdDosen(1);

        doThrow(new RuntimeException("Invalid MataKuliah ID")).when(mataKuliahValidation).validateAndGetMataKuliahById(any(Integer.class));

        assertThrows(RuntimeException.class, () -> mataKuliahService.updateMataKuliah(request, "token", -1));
    }

    @Test
    @DisplayName("Berhasil delete mata kuliah")
    void deleteMataKuliah_Success() {
        Integer id = 1;

        doNothing().when(mataKuliahRepository).deleteById(id);

        String response = mataKuliahService.deleteMataKuliah(id);

        assertNull(response);
    }

    @Test
    @DisplayName("Gagal delete mata kuliah karena mata kuliah id invalid")
    void deleteMataKuliah_InvalidMataKuliahId() {
        Integer id = -1;

        doThrow(new RuntimeException("Invalid MataKuliah ID")).when(mataKuliahValidation).validateAndGetMataKuliahById(any(Integer.class));

        assertThrows(RuntimeException.class, () -> mataKuliahService.deleteMataKuliah(id));
    }

    @Test
    @DisplayName("Berhasil map mata kuliah ke response")
    void mapToResponse_Success() {
        Dosen dosen = new Dosen();
        dosen.setNama("Dosen1");

        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setId(1);
        mataKuliah.setNamaMataKuliah("Math");
        mataKuliah.setSks(3);
        mataKuliah.setDosen(dosen);

        MataKuliahResponse response = mataKuliahService.mapToResponse(mataKuliah);

        assertEquals(mataKuliah.getId(), response.getId());
        assertEquals(mataKuliah.getNamaMataKuliah(), response.getNamaMataKuliah());
        assertEquals(mataKuliah.getSks(), response.getSks());
        assertEquals(mataKuliah.getDosen().getNama(), response.getNamaDosenMataKuliah());
    }

    @Test
    @DisplayName("Gagal map mata kuliah ke response karena mata kuliah null")
    void mapToResponse_NullMataKuliah() {
        assertThrows(NullPointerException.class, () -> mataKuliahService.mapToResponse(null));
    }

    @Test
    @DisplayName("Gagal map mata kuliah ke response karena dosen null")
    void mapToResponse_NullDosenInMataKuliah() {
        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setId(1);
        mataKuliah.setNamaMataKuliah("Math");
        mataKuliah.setSks(3);

        assertThrows(NullPointerException.class, () -> mataKuliahService.mapToResponse(mataKuliah));
    }
}