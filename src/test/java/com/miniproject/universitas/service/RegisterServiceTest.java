package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.register.RegisterRequest;
import com.miniproject.universitas.dto.register.RegisterResponse;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.UsersRepository;
import com.miniproject.universitas.validation.RegisterValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class RegisterServiceTest {

    @InjectMocks
    private RegisterService registerService;

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private RegisterValidation registerValidation;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Berhasil register user")
    void registerUser_ValidRequest_Success() {
        RegisterRequest request = new RegisterRequest();
        request.setEmail("test@test.com");
        request.setAlamat("Test Address");
        request.setPassword("password");
        request.setRole("admin");

        Users users = new Users();
        users.setEmail(request.getEmail());
        users.setAlamat(request.getAlamat());
        users.setPassword(request.getPassword());
        users.setRole(request.getRole());

        when(usersRepository.save(any(Users.class))).thenReturn(users);
        when(registerValidation.validateRoleAndCreateUser(request, users)).thenReturn(users);

        RegisterResponse response = registerService.registerUser(request);

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals("Register Success", response.getMessage());
    }

    @Test
    @DisplayName("Gagal register user karena email sudah terdaftar")
    void registerUser_InvalidEmail_ExceptionThrown() throws Exception {
        RegisterRequest request = new RegisterRequest();
        request.setEmail("invalidEmail");
        request.setAlamat("Test Address");
        request.setPassword("password");
        request.setRole("admin");

        doThrow(new IllegalArgumentException("Invalid email")).when(registerValidation).validateEmail(request.getEmail());

        assertThrows(IllegalArgumentException.class, () -> registerService.registerUser(request));
    }
}
