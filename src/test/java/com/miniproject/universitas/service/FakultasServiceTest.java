package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.fakultas.FakultasRequest;
import com.miniproject.universitas.dto.fakultas.FakultasResponse;
import com.miniproject.universitas.entity.Fakultas;
import com.miniproject.universitas.repository.FakultasRepository;
import com.miniproject.universitas.validation.FakultasValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class FakultasServiceTest {

    @InjectMocks
    private FakultasService fakultasService;

    @Mock
    private FakultasRepository fakultasRepository;

    @Mock
    private FakultasValidation fakultasValidation;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    //Create Fakultas
    @Test
    @DisplayName("Berhasil membuat fakultas karena token valid")
    void createFakultas_ValidToken_Success() {
        FakultasRequest request = new FakultasRequest();
        request.setNamaFakultas("Fakultas Teknik");

        Fakultas fakultas = new Fakultas();
        fakultas.setNamaFakultas(request.getNamaFakultas());

        when(fakultasRepository.save(any(Fakultas.class))).thenReturn(fakultas);

        FakultasResponse response = fakultasService.createFakultas(request, "validToken");

        assertEquals(request.getNamaFakultas(), response.getNamaFakultas());
    }

    @Test
    @DisplayName("Tidak Berhasil membuat fakultas karena token invalid")
    void createFakultas_InvalidToken_ExceptionThrown() {
        FakultasRequest request = new FakultasRequest();
        request.setNamaFakultas("Fakultas Teknik");

        doThrow(new IllegalArgumentException("Invalid token")).when(fakultasValidation).validateUser("invalidToken");

        assertThrows(IllegalArgumentException.class, () -> fakultasService.createFakultas(request, "invalidToken"));
    }

    //Get All Fakultas
    @Test
    @DisplayName("Berhasil membuat fakultas karena token valid")
    void createFakultas_ValidToken_Success2() {
        FakultasRequest request = new FakultasRequest();
        request.setNamaFakultas("Fakultas Teknik");

        Fakultas fakultas = new Fakultas();
        fakultas.setNamaFakultas(request.getNamaFakultas());

        doNothing().when(fakultasValidation).validateUser("validToken");
        when(fakultasRepository.save(any(Fakultas.class))).thenReturn(fakultas);

        FakultasResponse response = fakultasService.createFakultas(request, "validToken");

        assertEquals(request.getNamaFakultas(), response.getNamaFakultas());
    }

    @Test
    @DisplayName("Tidak Berhasil mendapatkan semua fakultas karena token invalid")
    void getAllFakultas_InvalidToken_ExceptionThrown() {
        doThrow(new IllegalArgumentException("Invalid token")).when(fakultasValidation).validateUser("invalidToken");

        assertThrows(IllegalArgumentException.class, () -> fakultasService.getAllFakultas("invalidToken"));
    }

    @Test
    @DisplayName("Tidak Berhasil mendapatkan semua fakultas karena tidak ada fakultas")
    void getAllFakultasReturnsEmptyListWhenNoFakultasFound() {
        when(fakultasRepository.findAll()).thenReturn(new ArrayList<>());

        List<FakultasResponse> responseList = fakultasService.getAllFakultas("validToken");

        assertTrue(responseList.isEmpty());
    }

    //Get Fakultas By Id
    @Test
    @DisplayName("Berhasil mendapatkan fakultas berdasarkan id")
    void getFakultasById_ValidIdAndToken_Success() {
        Integer id = 1;
        String token = "validToken";
        Fakultas fakultas = new Fakultas();

        when(fakultasRepository.findById(id)).thenReturn(Optional.of(fakultas));
        when(fakultasValidation.validateAndGetFakultasById(Optional.of(fakultas), id)).thenReturn(fakultas);

        FakultasResponse response = fakultasService.getFakultasById(id, token);

        assertEquals(fakultas.getIdFakultas(), response.getIdFakultas());
    }

    @Test
    @DisplayName("Tidak Berhasil mendapatkan fakultas berdasarkan id karena token invalid")
    void getFakultasById_InvalidToken_ExceptionThrown() {
        Integer id = 1;
        String token = "invalidToken";

        doThrow(new IllegalArgumentException("Invalid token")).when(fakultasValidation).validateUser(token);

        assertThrows(IllegalArgumentException.class, () -> fakultasService.getFakultasById(id, token));
    }

    @Test
    @DisplayName("Tidak Berhasil mendapatkan fakultas berdasarkan id karena id tidak ditemukan")
    void getFakultasById_NonExistingId_ExceptionThrown() {
        Integer id = 1;
        String token = "validToken";

        when(fakultasRepository.findById(id)).thenReturn(Optional.empty());
        doThrow(new NullPointerException("Fakultas not found")).when(fakultasValidation).validateAndGetFakultas(Optional.empty());

        assertThrows(NullPointerException.class, () -> fakultasService.getFakultasById(id, token));
    }

    //Update Fakultas By Id
    @Test
    @DisplayName("Berhasil update fakultas berdasarkan id, karena id dan token valid")
    void updateFakultasById_ValidIdRequestAndToken_Success() {
        Integer id = 1;
        String token = "validToken";
        FakultasRequest request = new FakultasRequest();
        request.setNamaFakultas("Fakultas Teknik");
        Fakultas fakultas = new Fakultas();

        when(fakultasRepository.findById(id)).thenReturn(Optional.of(fakultas));
        when(fakultasValidation.validateAndGetFakultasById(Optional.of(fakultas), id)).thenReturn(fakultas);
        when(fakultasRepository.save(fakultas)).thenReturn(fakultas);

        FakultasResponse response = fakultasService.updateFakultasById(id, request, token);

        assertEquals(request.getNamaFakultas(), response.getNamaFakultas());
    }

    @Test
    @DisplayName("Tiidak Berhasil update fakultas berdasarkan id, karena token invalid")
    void updateFakultasById_InvalidToken_ExceptionThrown() {
        Integer id = 1;
        String token = "invalidToken";
        FakultasRequest request = new FakultasRequest();
        request.setNamaFakultas("Fakultas Teknik");

        doThrow(new IllegalArgumentException("Invalid token")).when(fakultasValidation).validateUser(token);

        assertThrows(IllegalArgumentException.class, () -> fakultasService.updateFakultasById(id, request, token));
    }

    @Test
    @DisplayName("Tidak Berhasil update fakultas berdasarkan id, karena id tidak ditemukan")
    void updateFakultasById_NonExistingId_ExceptionThrown() {
        Integer id = 1;
        String token = "validToken";
        FakultasRequest request = new FakultasRequest();
        request.setNamaFakultas("Fakultas Teknik");

        when(fakultasRepository.findById(id)).thenReturn(Optional.empty());
        doThrow(new IllegalArgumentException("Fakultas not found")).when(fakultasValidation).validateAndGetFakultasById(Optional.empty(), id);

        assertThrows(IllegalArgumentException.class, () -> fakultasService.updateFakultasById(id, request, token));
    }
    //Delete Fakultas By Id
    @Test
    @DisplayName("Berhasil delete fakultas berdasarkan id, karena id dan token valid")
    void deleteFakultasById_ValidIdAndToken_Success() {
        Integer id = 1;
        String token = "validToken";

        doNothing().when(fakultasRepository).deleteById(id);

        String response = fakultasService.deleteFakultasById(id, token);

        assertNull(response);
        verify(fakultasValidation, times(1)).validateUser(token);
        verify(fakultasRepository, times(1)).deleteById(id);
    }

    @Test
    @DisplayName("Tidak Berhasil delete fakultas berdasarkan id, karena token invalid")
    void deleteFakultasById_InvalidToken_ExceptionThrown() {
        Integer id = 1;
        String token = "invalidToken";

        doThrow(new IllegalArgumentException("Invalid token")).when(fakultasValidation).validateUser(token);

        assertThrows(IllegalArgumentException.class, () -> fakultasService.deleteFakultasById(id, token));
    }

    @Test
    @DisplayName("Tidak Berhasil delete fakultas berdasarkan id, karena id tidak ditemukan")
    void deleteFakultasById_NonExistingId_ExceptionThrown() {
        Integer id = 1;
        String token = "validToken";

        doThrow(new EmptyResultDataAccessException(1)).when(fakultasRepository).deleteById(id);

        assertThrows(EmptyResultDataAccessException.class, () -> fakultasService.deleteFakultasById(id, token));
    }
}
