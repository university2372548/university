package com.miniproject.universitas.service;
import com.miniproject.universitas.dto.login.LoginRequest;
import com.miniproject.universitas.dto.login.LoginResponse;
import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.entity.Users;
import com.miniproject.universitas.repository.DosenRepository;
import com.miniproject.universitas.repository.MahasiswaRepository;
import com.miniproject.universitas.repository.UsersRepository;
import com.miniproject.universitas.validation.UserValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class LoginServiceTest {

    @InjectMocks
    private LoginService loginService;

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private UserValidation userValidation;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private DosenRepository dosenRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Berhasil login dengan role Dosen")
    void login_ValidDosenRole_Success() {
        LoginRequest request = new LoginRequest();
        request.setNomorInduk("12345");
        request.setPassword("password");

        Users user = new Users();
        user.setNomorInduk(request.getNomorInduk());
        user.setPassword(request.getPassword());
        user.setRole("dosen");

        Dosen dosen = new Dosen();
        dosen.setId(1);

        when(userValidation.validateUser(request.getNomorInduk(), request.getPassword())).thenReturn(user);
        when(usersRepository.save(any(Users.class))).thenReturn(user);
        when(dosenRepository.findByUsersId(user.getId())).thenReturn(Optional.of(dosen));

        LoginResponse response = loginService.login(request);

        assertEquals(user.getRole(), response.getRole());
        assertEquals(dosen.getId(), response.getId());
        assertNotNull(response.getToken());
    }

    @Test
    @DisplayName("Berhasil login dengan role Mahasiswa")
    void login_ValidMahasiswaRole_Success() {
        LoginRequest request = new LoginRequest();
        request.setNomorInduk("12345");
        request.setPassword("password");

        Users user = new Users();
        user.setNomorInduk(request.getNomorInduk());
        user.setPassword(request.getPassword());
        user.setRole("mahasiswa");

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(1);

        when(userValidation.validateUser(request.getNomorInduk(), request.getPassword())).thenReturn(user);
        when(usersRepository.save(any(Users.class))).thenReturn(user);
        when(mahasiswaRepository.findByUsersId(user.getId())).thenReturn(Optional.of(mahasiswa));

        LoginResponse response = loginService.login(request);

        assertEquals(user.getRole(), response.getRole());
        assertEquals(mahasiswa.getId(), response.getId());
        assertNotNull(response.getToken());
    }

    @Test
    @DisplayName("Gagal login dengan role Dosen")
    void login_NonExistingDosenRole_ExceptionThrown() {
        LoginRequest request = new LoginRequest();
        request.setNomorInduk("12345");
        request.setPassword("password");

        Users user = new Users();
        user.setNomorInduk(request.getNomorInduk());
        user.setPassword(request.getPassword());
        user.setRole("dosen");

        when(userValidation.validateUser(request.getNomorInduk(), request.getPassword())).thenReturn(user);
        when(usersRepository.save(any(Users.class))).thenReturn(user);
        when(dosenRepository.findByUsersId(user.getId())).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> loginService.login(request));
    }

    @Test
    @DisplayName("Gagal login dengan role Mahasiswa")
    void login_NonExistingMahasiswaRole_ExceptionThrown() {
        LoginRequest request = new LoginRequest();
        request.setNomorInduk("12345");
        request.setPassword("password");

        Users user = new Users();
        user.setNomorInduk(request.getNomorInduk());
        user.setPassword(request.getPassword());
        user.setRole("mahasiswa");

        when(userValidation.validateUser(request.getNomorInduk(), request.getPassword())).thenReturn(user);
        when(usersRepository.save(any(Users.class))).thenReturn(user);
        when(mahasiswaRepository.findByUsersId(user.getId())).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> loginService.login(request));
    }
}
