package com.miniproject.universitas.service;

import com.miniproject.universitas.repository.UsersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

class TokenServiceTest {

    @InjectMocks
    private TokenService tokenService;

    @Mock
    private UsersRepository usersRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Berhasil mendapatkan token")
    void getToken_ValidToken_Success() {
        String token = "validToken";

        when(usersRepository.findByToken(token)).thenReturn(java.util.Optional.ofNullable(new com.miniproject.universitas.entity.Users()));

        boolean result = tokenService.getToken(token);

        assertTrue(result);
    }

    @Test
    @DisplayName("Gagal mendapatkan token")
    void getToken_InvalidToken_Failure() {
        String token = "invalidToken";

        when(usersRepository.findByToken(token)).thenReturn(java.util.Optional.empty());

        boolean result = tokenService.getToken(token);

        assertFalse(result);
    }
}