package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.krs.KrsDetailNilai;
import com.miniproject.universitas.dto.krs.KrsReport;
import com.miniproject.universitas.dto.krs.KrsRequest;
import com.miniproject.universitas.dto.krs.KrsResponse;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.repository.KrsRepository;
import com.miniproject.universitas.validation.KrsValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.never;

class KrsServiceTest {

    @InjectMocks
    KrsService krsService;

    @Mock
    KrsRepository krsRepository;

    @Mock
    KrsValidation krsValidation;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    //Create KRS
    @Test
    @DisplayName("Berhasil membuat KRS")
    void createKrsSuccessfully() {
        KrsRequest krsRequest = new KrsRequest();
        String token = "validToken";
        when(krsValidation.validateKrsRequest(krsRequest)).thenReturn(new Mahasiswa());
        when(krsValidation.validateAndCreateKrs(any(KrsRequest.class), any(Mahasiswa.class))).thenReturn(new KrsResponse());

        krsService.createKrs(krsRequest, token);

        verify(krsValidation, times(1)).validateUser(token);
        verify(krsValidation, times(1)).validateKrsRequest(krsRequest);
        verify(krsValidation, times(1)).validateAndCreateKrs(any(KrsRequest.class), any(Mahasiswa.class));
    }

    @Test
    @DisplayName("Gagal membuat KRS dengan token yang tidak valid")
    void createKrsWithInvalidToken() {
        KrsRequest krsRequest = new KrsRequest();
        String invalidToken = "invalidToken";
        when(krsValidation.validateUser(invalidToken)).thenThrow(new IllegalArgumentException("Invalid token"));

        assertThrows(IllegalArgumentException.class, () -> krsService.createKrs(krsRequest, invalidToken));

        verify(krsValidation, times(1)).validateUser(invalidToken);
        verify(krsValidation, never()).validateKrsRequest(krsRequest);
        verify(krsValidation, never()).validateAndCreateKrs(any(KrsRequest.class), any(Mahasiswa.class));
    }

    //Get KRS by ID
    @Test
    @DisplayName("Berhasil mendapatkan KRS dengan ID yang valid")
    void getKrsByIdSuccessfully() {
        Integer id = 1;
        KrsResponse expectedResponse = new KrsResponse();
        when(krsValidation.getKrsDetails(id)).thenReturn(expectedResponse);

        KrsResponse actualResponse = krsService.getKrsById(id);

        assertEquals(expectedResponse, actualResponse);
        verify(krsValidation, times(1)).getKrsDetails(id);
    }

    @Test
    @DisplayName("Gagal mendapatkan KRS dengan ID yang tidak valid")
    void getKrsByIdWithNonExistentId() {
        Integer id = 999;
        when(krsValidation.getKrsDetails(id)).thenThrow(new IllegalArgumentException("KRS not found"));

        assertThrows(IllegalArgumentException.class, () -> krsService.getKrsById(id));
        verify(krsValidation, times(1)).getKrsDetails(id);
    }

    //Update KRS
    @Test
    @DisplayName("Berhasil mengupdate KRS")
    void updateKrsSuccessfully() {
        Integer id = 1;
        String token = "validToken";
        KrsRequest krsRequest = new KrsRequest();
        KrsResponse expectedResponse = new KrsResponse();
        when(krsValidation.validateUserAndMahasiswa(token, krsRequest)).thenReturn(new Mahasiswa());
        when(krsValidation.validateAndUpdateKrs(eq(id), any(Mahasiswa.class), eq(krsRequest))).thenReturn(expectedResponse);
        KrsResponse actualResponse = krsService.updateKrs(id, krsRequest, token);

        assertEquals(expectedResponse, actualResponse);
        verify(krsValidation, times(1)).validateUserAndMahasiswa(token, krsRequest);
        verify(krsValidation, times(1)).validateAndUpdateKrs(eq(id), any(Mahasiswa.class), eq(krsRequest));    }
    @Test
    @DisplayName("Gagal mengupdate KRS dengan token yang tidak valid")
    void updateKrsWithNonExistentId() {
        Integer id = 999;
        String token = "validToken";
        KrsRequest krsRequest = new KrsRequest();
        when(krsValidation.validateUserAndMahasiswa(token, krsRequest)).thenReturn(new Mahasiswa());
        when(krsValidation.validateAndUpdateKrs(eq(id), any(Mahasiswa.class), eq(krsRequest))).thenThrow(new IllegalArgumentException("KRS not found"));
        assertThrows(IllegalArgumentException.class, () -> krsService.updateKrs(id, krsRequest, token));
        verify(krsValidation, times(1)).validateUserAndMahasiswa(token, krsRequest);
        when(krsValidation.validateAndUpdateKrs(eq(id), any(Mahasiswa.class), eq(krsRequest))).thenThrow(new IllegalArgumentException("KRS not found"));
    }

    @Test
    @DisplayName("Gagal mengupdate KRS dengan token yang tidak valid")
    void updateKrsWithInvalidToken() {
        Integer id = 1;
        String token = "invalidToken";
        KrsRequest krsRequest = new KrsRequest();
        when(krsValidation.validateUserAndMahasiswa(token, krsRequest)).thenThrow(new IllegalArgumentException("Invalid token"));
        assertThrows(IllegalArgumentException.class, () -> krsService.updateKrs(id, krsRequest, token));
        verify(krsValidation, times(1)).validateUserAndMahasiswa(token, krsRequest);
        verify(krsValidation, never()).validateAndUpdateKrs(eq(id), any(Mahasiswa.class), eq(krsRequest));    }

    //Delete KRS
    @Test
    @DisplayName("Berhasil menghapus KRS")
    void deleteKrsByIdSuccessfully() {
        Integer id = 1;
        String token = "validToken";
        doNothing().when(krsValidation).validateUserAccess(token);
        doNothing().when(krsRepository).deleteById(id);

        String response = krsService.deleteKrsById(id, token);

        assertEquals("KRS Berhasil dihapus", response);
        verify(krsValidation, times(1)).validateUserAccess(token);
        verify(krsRepository, times(1)).deleteById(id);
    }

    @Test
    @DisplayName("Gagal menghapus KRS dengan ID yang tidak valid")
    void deleteKrsByIdWithNonExistentId() {
        Integer id = 999;
        String token = "validToken";
        doNothing().when(krsValidation).validateUserAccess(token);
        doThrow(new IllegalArgumentException("KRS not found")).when(krsRepository).deleteById(id);

        assertThrows(IllegalArgumentException.class, () -> krsService.deleteKrsById(id, token));
        verify(krsValidation, times(1)).validateUserAccess(token);
        verify(krsRepository, times(1)).deleteById(id);
    }

    @Test
    @DisplayName("Gagal menghapus KRS dengan token yang tidak valid")
    void deleteKrsByIdWithInvalidToken() {
        Integer id = 1;
        String token = "invalidToken";
        doThrow(new IllegalArgumentException("Invalid token")).when(krsValidation).validateUserAccess(token);

        assertThrows(IllegalArgumentException.class, () -> krsService.deleteKrsById(id, token));
        verify(krsValidation, times(1)).validateUserAccess(token);
        verify(krsRepository, never()).deleteById(id);
    }

    @Test
    @DisplayName("Berhasil mendapatkan KRS report dengan ID Mahasiswa yang valid")
    void getKrsReportByIdMahasiswaSuccessfully() {
        Integer id = 1;
        List<KrsReport> expectedReport = new ArrayList<>();
        when(krsRepository.findKrsDetailsByMahasiswaId(id)).thenReturn(expectedReport);

        List<KrsReport> actualReport = krsService.getKrsReportByIdMahasiswa(id);

        assertEquals(expectedReport, actualReport);
        verify(krsRepository, times(1)).findKrsDetailsByMahasiswaId(id);
    }

    @Test
    @DisplayName("Gagal mendapatkan KRS report dengan ID Mahasiswa yang tidak valid")
    void getKrsReportByIdMahasiswaWithNonExistentId() {
        Integer id = 999;
        List<KrsReport> expectedReport = new ArrayList<>();
        when(krsRepository.findKrsDetailsByMahasiswaId(id)).thenReturn(expectedReport);

        List<KrsReport> actualReport = krsService.getKrsReportByIdMahasiswa(id);

        assertTrue(actualReport.isEmpty());
        verify(krsRepository, times(1)).findKrsDetailsByMahasiswaId(id);
    }

    //GetAll KRS
    @Test
    @DisplayName("getAllKrs returns correct KrsDetailNilai list when KrsDetailNilai exist")
    void getAllKrsReturnsCorrectListWhenKrsDetailNilaiExist() {
        KrsDetailNilai krsDetailNilai1 = new KrsDetailNilai(1, "string1", "string2", "string3", 1);
        KrsDetailNilai krsDetailNilai2 = new KrsDetailNilai(2, "string4", "string5", "string6", 2);
        List<KrsDetailNilai> expectedList = Arrays.asList(krsDetailNilai1, krsDetailNilai2);

        when(krsRepository.findAllKrs()).thenReturn(expectedList);

        List<KrsDetailNilai> actualList = krsService.getAllKrs();

        assertEquals(expectedList, actualList);
    }

    @Test
    @DisplayName("getAllKrs returns empty list when no KrsDetailNilai")
    void getAllKrsReturnsEmptyListWhenNoKrsDetailNilai() {
        when(krsRepository.findAllKrs()).thenReturn(Collections.emptyList());

        List<KrsDetailNilai> actualList = krsService.getAllKrs();

        assertEquals(Collections.emptyList(), actualList);
    }

    //GetAll KRS by NID
    @Test
    @DisplayName("Berhasil mendapatkan KRS dengan NID yang valid")
    void getAllKrsByNidReturnsCorrectListWhenKrsDetailNilaiWithMatchingNidExist() {
        String nid = "validNid";
        KrsDetailNilai krsDetailNilai1 = new KrsDetailNilai(1, "string1", nid, "string3", 1);
        KrsDetailNilai krsDetailNilai2 = new KrsDetailNilai(2, "string4", nid, "string6", 2);
        List<KrsDetailNilai> expectedList = Arrays.asList(krsDetailNilai1, krsDetailNilai2);

        when(krsRepository.findAllKrs()).thenReturn(Arrays.asList(krsDetailNilai1, krsDetailNilai2));

        List<KrsDetailNilai> actualList = krsService.getAllKrsByNid(nid);

        assertEquals(expectedList, actualList);
    }

    @Test
    @DisplayName("Gagal mendapatkan list KRS dengan nid yang valid tapi KrsDetailNilai list kosong")
    void getAllKrsByNidReturnsEmptyListWhenNoKrsDetailNilaiWithMatchingNid() {
        String nid = "validNid";
        KrsDetailNilai krsDetailNilai1 = new KrsDetailNilai(1, "string1", "differentNid", "string3", 1);
        KrsDetailNilai krsDetailNilai2 = new KrsDetailNilai(2, "string4", "differentNid", "string6", 2);

        when(krsRepository.findAllKrs()).thenReturn(Arrays.asList(krsDetailNilai1, krsDetailNilai2));

        List<KrsDetailNilai> actualList = krsService.getAllKrsByNid(nid);

        assertTrue(actualList.isEmpty());
    }

    @Test
    @DisplayName("Gagal mendapatkan list KRS dengan nid yang tidak valid")
    void getAllKrsByNidReturnsEmptyListWhenKrsDetailNilaiListIsEmpty() {
        String nid = "validNid";

        when(krsRepository.findAllKrs()).thenReturn(Collections.emptyList());

        List<KrsDetailNilai> actualList = krsService.getAllKrsByNid(nid);

        assertTrue(actualList.isEmpty());
    }
}