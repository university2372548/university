package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.mahasiswa.MahasiswaRequest;
import com.miniproject.universitas.dto.mahasiswa.MahasiswaResponse;
import com.miniproject.universitas.entity.Fakultas;
import com.miniproject.universitas.entity.Jurusan;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.repository.JurusanRepository;
import com.miniproject.universitas.repository.MahasiswaRepository;
import com.miniproject.universitas.validation.MahasiswaValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class MahasiswaServiceTest {

    @InjectMocks
    private MahasiswaService mahasiswaService;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private JurusanRepository jurusanRepository;

    @Mock
    private MahasiswaValidation mahasiswaValidation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    //createMahasiswa
    @Test
    @DisplayName("Successfully creates Mahasiswa with valid request and token")
    void createMahasiswa_ValidRequestAndToken_Success() {
        MahasiswaRequest request = new MahasiswaRequest();
        request.setIdMahasiswa(1);
        request.setIdJurusan(1);

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(request.getIdMahasiswa());

        Jurusan jurusan = new Jurusan();
        Fakultas fakultas = new Fakultas(); // Ensure that Fakultas is not null
        jurusan.setFakultas(fakultas);
        jurusan.setIdJurusan(request.getIdJurusan());

        doNothing().when(mahasiswaValidation).validateUser("validToken");
        when(mahasiswaValidation.validateMahasiswaExists(request.getIdMahasiswa())).thenReturn(mahasiswa);
        when(jurusanRepository.findById(request.getIdJurusan())).thenReturn(Optional.of(jurusan));
        when(mahasiswaRepository.save(any(Mahasiswa.class))).thenAnswer(invocation -> {
            Mahasiswa arg = invocation.getArgument(0);
            arg.setId(request.getIdMahasiswa()); // Set the id before saving
            return arg;
        });

        MahasiswaResponse response = mahasiswaService.createMahasiswa(request, "validToken");

        assertEquals(request.getIdMahasiswa(), response.getIdMahasiswa());
        // Add null check before calling getNamaFakultas()
            assertEquals(jurusan.getFakultas().getNamaFakultas(), response.getNamaFakultas());
        }

    @Test
    @DisplayName("Tidak dapat membuat Mahasiswa dengan token invalid")
    void createMahasiswa_InvalidToken_ExceptionThrown() {
        MahasiswaRequest request = new MahasiswaRequest();
        request.setIdMahasiswa(1);
        request.setIdJurusan(1);

        doThrow(new IllegalArgumentException("Invalid token")).when(mahasiswaValidation).validateUser("invalidToken");

        assertThrows(IllegalArgumentException.class, () -> mahasiswaService.createMahasiswa(request, "invalidToken"));
    }

    @Test
    @DisplayName("Tidak dapat membuat Mahasiswa dengan id Mahasiswa yang sudah ada")
    void createMahasiswa_NonExistingJurusanId_ExceptionThrown() {
        MahasiswaRequest request = new MahasiswaRequest();
        request.setIdMahasiswa(1);
        request.setIdJurusan(1);

        when(jurusanRepository.findById(request.getIdJurusan())).thenReturn(Optional.empty());

        assertThrows(NullPointerException.class, () -> mahasiswaService.createMahasiswa(request, "validToken"));
    }

    //getMahasiswaAll
    @Test
    @DisplayName("Berhasil mendapatkan semua Mahasiswa dengan token valid")
    void getMahasiswaAll_ValidToken_Success() {
        Mahasiswa mahasiswa1 = new Mahasiswa();
        mahasiswa1.setNama("Mahasiswa 1");
        Jurusan jurusan1 = new Jurusan();
        Fakultas fakultas1 = new Fakultas(); // Ensure that Fakultas is not null
        jurusan1.setFakultas(fakultas1);
        mahasiswa1.setJurusan(jurusan1);

        Mahasiswa mahasiswa2 = new Mahasiswa();
        mahasiswa2.setNama("Mahasiswa 2");
        Jurusan jurusan2 = new Jurusan();
        Fakultas fakultas2 = new Fakultas(); // Ensure that Fakultas is not null
        jurusan2.setFakultas(fakultas2);
        mahasiswa2.setJurusan(jurusan2);

        List<Mahasiswa> mahasiswaList = List.of(mahasiswa1, mahasiswa2);

        when(mahasiswaRepository.findAll()).thenReturn(mahasiswaList);

        List<MahasiswaResponse> responseList = mahasiswaService.getMahasiswaAll("validToken");

        assertEquals(2, responseList.size());
        if (mahasiswa1.getJurusan() != null && mahasiswa1.getJurusan().getFakultas() != null) {
            assertEquals(mahasiswa1.getJurusan().getFakultas().getNamaFakultas(), responseList.get(0).getNamaFakultas());
        }
        if (mahasiswa2.getJurusan() != null && mahasiswa2.getJurusan().getFakultas() != null) {
            assertEquals(mahasiswa2.getJurusan().getFakultas().getNamaFakultas(), responseList.get(1).getNamaFakultas());
        }
    }

    @Test
    @DisplayName("Tidak dapat mendapatkan semua Mahasiswa dengan token invalid")
    void getMahasiswaAll_InvalidToken_ExceptionThrown() {
        doThrow(new IllegalArgumentException("Invalid token")).when(mahasiswaValidation).validateUser("invalidToken");

        assertThrows(IllegalArgumentException.class, () -> mahasiswaService.getMahasiswaAll("invalidToken"));
    }

    @Test
    @DisplayName("Tidak dapat mendapatkan semua Mahasiswa ketika tidak ada Mahasiswa")
    void getMahasiswaAllReturnsEmptyListWhenNoMahasiswaFound() {
        when(mahasiswaRepository.findAll()).thenReturn(new ArrayList<>());

        List<MahasiswaResponse> responseList = mahasiswaService.getMahasiswaAll("validToken");

        assertTrue(responseList.isEmpty());
    }

    //getMahasiswaById
    @Test
    @DisplayName("Berhasil mendapatkan Mahasiswa by id dengan token valid")
    void getMahasiswaById_ValidIdAndToken_Success() {
        Integer id = 1;
        String token = "validToken";
        Mahasiswa mahasiswa = new Mahasiswa();
        Jurusan jurusan = new Jurusan();
        Fakultas fakultas = new Fakultas(); // Ensure that Fakultas is not null
        jurusan.setFakultas(fakultas);
        mahasiswa.setJurusan(jurusan);

        doNothing().when(mahasiswaValidation).validateUserRole(token);
        when(mahasiswaValidation.validateMahasiswaExists(id)).thenReturn(mahasiswa);

        MahasiswaResponse response = mahasiswaService.getMahasiswaById(id, token);

        // Add null check before calling getNamaFakultas()
        if (mahasiswa.getJurusan() != null && mahasiswa.getJurusan().getFakultas() != null) {
            assertEquals(mahasiswa.getJurusan().getFakultas().getNamaFakultas(), response.getNamaFakultas());
        }
    }

    @Test
    @DisplayName("Tidak dapat mendapatkan Mahasiswa by id dengan token invalid")
    void getMahasiswaById_InvalidToken_ExceptionThrown() {
        Integer id = 1;
        String token = "invalidToken";

        doThrow(new IllegalArgumentException("Invalid token")).when(mahasiswaValidation).validateUserRole(token);

        assertThrows(IllegalArgumentException.class, () -> mahasiswaService.getMahasiswaById(id, token));
    }

    @Test
    @DisplayName("Tidak dapat mendapatkan Mahasiswa by id dengan id yang tidak ada")
    void getMahasiswaById_NonExistingId_ExceptionThrown() {
        Integer id = 1;
        String token = "validToken";

        when(mahasiswaValidation.validateMahasiswaExists(id)).thenThrow(new NoSuchElementException("Mahasiswa not found"));

        assertThrows(NoSuchElementException.class, () -> mahasiswaService.getMahasiswaById(id, token));
    }

    //updateMahasiswa
    @Test
    @DisplayName("Berhasil update Mahasiswa by id with valid request and token")
    void updateMahasiswa_ValidIdRequestAndToken_Success() {
        Integer id = 1;
        String token = "validToken";
        MahasiswaRequest request = new MahasiswaRequest();
        request.setNama("Updated Mahasiswa");
        request.setIdJurusan(1);

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setNama("Test Mahasiswa");

        Jurusan jurusan = new Jurusan();
        jurusan.setIdJurusan(request.getIdJurusan());
        Fakultas fakultas = new Fakultas(); // Ensure that Fakultas is not null
        jurusan.setFakultas(fakultas);

        doNothing().when(mahasiswaValidation).validateUser(token);
        when(mahasiswaValidation.validateMahasiswaExists(id)).thenReturn(mahasiswa);
        when(jurusanRepository.findById(request.getIdJurusan())).thenReturn(Optional.of(jurusan));
        when(mahasiswaRepository.save(any(Mahasiswa.class))).thenReturn(mahasiswa);

        MahasiswaResponse response = mahasiswaService.updateMahasiswa(id, request, token);

        assertEquals(request.getNama(), response.getNama());
        if (jurusan.getFakultas() != null) {
            assertEquals(jurusan.getFakultas().getNamaFakultas(), response.getNamaFakultas());
        }
    }

    @Test
    @DisplayName("Tidak dapat update Mahasiswa by id with invalid token")
    void updateMahasiswa_InvalidToken_ExceptionThrown() {
        Integer id = 1;
        String token = "invalidToken";
        MahasiswaRequest request = new MahasiswaRequest();
        request.setNama("Updated Mahasiswa");
        request.setIdJurusan(1);

        doThrow(new IllegalArgumentException("Invalid token")).when(mahasiswaValidation).validateUser(token);

        assertThrows(IllegalArgumentException.class, () -> mahasiswaService.updateMahasiswa(id, request, token));
    }

    @Test
    @DisplayName("Tidak dapat update Mahasiswa by id with non-existing id")
    void updateMahasiswa_NonExistingId_ExceptionThrown() {
        Integer id = 1;
        String token = "validToken";
        MahasiswaRequest request = new MahasiswaRequest();
        request.setNama("Updated Mahasiswa");
        request.setIdJurusan(1);

        when(mahasiswaValidation.validateMahasiswaExists(id)).thenThrow(new NoSuchElementException("Mahasiswa not found"));

        assertThrows(NoSuchElementException.class, () -> mahasiswaService.updateMahasiswa(id, request, token));
    }

    //deleteMahasiswa
    @Test
    @DisplayName("Berhasil delete Mahasiswa by id with valid id and token")
    void deleteMahasiswa_ValidIdAndToken_Success() {
        Integer id = 1;
        String token = "validToken";

        doNothing().when(mahasiswaValidation).validateUser(token);
        doNothing().when(mahasiswaRepository).deleteById(id);

        String response = mahasiswaService.deleteMahasiswa(id, token);

        assertNull(response);
    }

    @Test
    @DisplayName("Tidak dapat delete Mahasiswa by id with invalid token")
    void deleteMahasiswa_InvalidToken_ExceptionThrown() {
        Integer id = 1;
        String token = "invalidToken";

        doThrow(new IllegalArgumentException("Invalid token")).when(mahasiswaValidation).validateUser(token);

        assertThrows(IllegalArgumentException.class, () -> mahasiswaService.deleteMahasiswa(id, token));
    }

    @Test
    @DisplayName("Tidak dapat delete Mahasiswa by id with non-existing id")
    void deleteMahasiswa_NonExistingId_ExceptionThrown() {
        Integer id = 1;
        String token = "validToken";

        doThrow(new EmptyResultDataAccessException(1)).when(mahasiswaRepository).deleteById(id);

        assertThrows(EmptyResultDataAccessException.class, () -> mahasiswaService.deleteMahasiswa(id, token));
    }

    @Test
    @DisplayName("mapMahasiswaToResponse returns correct MahasiswaResponse when Jurusan and Fakultas are not null")
    void mapMahasiswaToResponseReturnsCorrectResponseWhenJurusanAndFakultasAreNotNull() {
        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(1);
        mahasiswa.setNim("12345");
        mahasiswa.setNama("Test Mahasiswa");

        Jurusan jurusan = new Jurusan();
        jurusan.setNamaJurusan("Test Jurusan");
        Fakultas fakultas = new Fakultas();
        fakultas.setNamaFakultas("Test Fakultas");
        jurusan.setFakultas(fakultas);
        mahasiswa.setJurusan(jurusan);

        MahasiswaResponse actualResponse = mahasiswaService.mapMahasiswaToResponse(mahasiswa);

        assertEquals(mahasiswa.getId(), actualResponse.getIdMahasiswa());
        assertEquals(mahasiswa.getNim(), actualResponse.getNim());
        assertEquals(mahasiswa.getNama(), actualResponse.getNama());
        assertEquals(jurusan.getNamaJurusan(), actualResponse.getNamaJurusan());
        assertEquals(fakultas.getNamaFakultas(), actualResponse.getNamaFakultas());
    }

    @Test
    @DisplayName("mapMahasiswaToResponse returns correct MahasiswaResponse when Jurusan is null")
    void mapMahasiswaToResponseReturnsCorrectResponseWhenJurusanIsNull() {
        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(1);
        mahasiswa.setNim("12345");
        mahasiswa.setNama("Test Mahasiswa");
        mahasiswa.setJurusan(null);

        MahasiswaResponse actualResponse = mahasiswaService.mapMahasiswaToResponse(mahasiswa);

        assertEquals(mahasiswa.getId(), actualResponse.getIdMahasiswa());
        assertEquals(mahasiswa.getNim(), actualResponse.getNim());
        assertEquals(mahasiswa.getNama(), actualResponse.getNama());
        assertEquals("Jurusan Belum di Daftarkan", actualResponse.getNamaJurusan());
        assertEquals("Fakultas Belum di Daftarkan", actualResponse.getNamaFakultas());
    }

    @Test
    @DisplayName("mapMahasiswaToResponse returns correct MahasiswaResponse when Fakultas is null")
    void mapMahasiswaToResponseReturnsCorrectResponseWhenFakultasIsNull() {
        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.setId(1);
        mahasiswa.setNim("12345");
        mahasiswa.setNama("Test Mahasiswa");

        Jurusan jurusan = new Jurusan();
        jurusan.setNamaJurusan("Test Jurusan");
        jurusan.setFakultas(null);
        mahasiswa.setJurusan(jurusan);

        MahasiswaResponse actualResponse = mahasiswaService.mapMahasiswaToResponse(mahasiswa);

        assertEquals(mahasiswa.getId(), actualResponse.getIdMahasiswa());
        assertEquals(mahasiswa.getNim(), actualResponse.getNim());
        assertEquals(mahasiswa.getNama(), actualResponse.getNama());
        assertEquals(jurusan.getNamaJurusan(), actualResponse.getNamaJurusan());
        assertEquals("Fakultas Belum di Daftarkan", actualResponse.getNamaFakultas());
    }
}

