package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.dosen.DosenResponse;
import com.miniproject.universitas.entity.Dosen;
import com.miniproject.universitas.repository.DosenRepository;
import com.miniproject.universitas.validation.DosenValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class DosenServiceTest {

    @InjectMocks
    private DosenService dosenService;

    @Mock
    private DosenRepository dosenRepository;

    @Mock
    private DosenValidation dosenValidation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Berhasil mendapatkan dosen berdasarkan id")
    void getDosenByIdReturnsCorrectDosen() {
        Dosen dosen = new Dosen();
        dosen.setId(1);
        dosen.setNid("123");
        dosen.setNama("John Doe");

        when(dosenValidation.validateDosenExists(1)).thenReturn(dosen);

        DosenResponse response = dosenService.getDosenById(1, "token");

        assertEquals(1, response.getIdDosen());
        assertEquals("123", response.getNid());
        assertEquals("John Doe", response.getNama());

        verify(dosenValidation, times(1)).validateUser("token");
        verify(dosenValidation, times(1)).validateDosenExists(1);
    }

    @Test
    @DisplayName("Gagal mendapatkan dosen berdasarkan id karena id tidak ditemukan")
    void getDosenByIdThrowsExceptionForInvalidId() {
        when(dosenValidation.validateDosenExists(1)).thenThrow(new RuntimeException("Dosen not found"));

        assertThrows(RuntimeException.class, () -> dosenService.getDosenById(1, "token"));

        verify(dosenValidation, times(1)).validateUser("token");
        verify(dosenValidation, times(1)).validateDosenExists(1);
    }

    @Test
    @DisplayName("Gagal mendapatkan dosen berdasarkan id karena token tidak valid")
    void getDosenByIdThrowsExceptionForInvalidToken() {
        doThrow(new RuntimeException("Invalid token")).when(dosenValidation).validateUser("token");

        assertThrows(RuntimeException.class, () -> dosenService.getDosenById(1, "token"));

        verify(dosenValidation, times(1)).validateUser("token");
    }

    @Test
    @DisplayName("Berhasil mendapatkan semua dosen")
    void getDosenAllReturnsCorrectDosenList() {
        Dosen dosen1 = new Dosen();
        dosen1.setId(1);
        dosen1.setNid("123");
        dosen1.setNama("John Doe");

        Dosen dosen2 = new Dosen();
        dosen2.setId(2);
        dosen2.setNid("456");
        dosen2.setNama("Jane Doe");

        List<Dosen> dosenList = Arrays.asList(dosen1, dosen2);

        when(dosenRepository.findAll()).thenReturn(dosenList);

        List<DosenResponse> responseList = dosenService.getDosenAll("token");

        assertEquals(2, responseList.size());
        assertEquals(1, responseList.get(0).getIdDosen());
        assertEquals("123", responseList.get(0).getNid());
        assertEquals("John Doe", responseList.get(0).getNama());
        assertEquals(2, responseList.get(1).getIdDosen());
        assertEquals("456", responseList.get(1).getNid());
        assertEquals("Jane Doe", responseList.get(1).getNama());

        verify(dosenValidation, times(1)).validateUser("token");
    }

    @Test
    @DisplayName("Gagal mendapatkan semua dosen karena token tidak valid")
    void getDosenAllThrowsExceptionForInvalidToken() {
        doThrow(new RuntimeException("Invalid token")).when(dosenValidation).validateUser("token");

        assertThrows(RuntimeException.class, () -> dosenService.getDosenAll("token"));

        verify(dosenValidation, times(1)).validateUser("token");
    }
}
