package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.nilai.NilaiReport;
import com.miniproject.universitas.dto.nilai.NilaiRequest;
import com.miniproject.universitas.entity.Krs;
import com.miniproject.universitas.entity.Mahasiswa;
import com.miniproject.universitas.entity.MataKuliah;
import com.miniproject.universitas.entity.Nilai;
import com.miniproject.universitas.repository.NilaiRepository;
import com.miniproject.universitas.validation.NilaiValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class NilaiServiceTest {

    @InjectMocks
    NilaiService nilaiService;

    @Mock
    NilaiRepository nilaiRepository;

    @Mock
    NilaiValidation nilaiValidation;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    //Create Nilai
    @Test
    @DisplayName("Berhasil membuat Nilai")
    void createNilaiWithValidRequestAndToken() {
        NilaiRequest nilaiRequest = new NilaiRequest();
        nilaiRequest.setKuis(BigDecimal.valueOf(90.0));
        nilaiRequest.setMidTest(BigDecimal.valueOf(85.0));
        nilaiRequest.setFinalTest(BigDecimal.valueOf(88.0));
        String validToken = "validToken";
        String invalidToken = "invalidToken";
        doThrow(new IllegalArgumentException("Invalid token")).when(nilaiValidation).validateUserRole(invalidToken);

        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setSks(3);

        Mahasiswa mahasiswa = new Mahasiswa();

        Krs krs = new Krs();
        krs.setMataKuliah(mataKuliah);
        krs.setMahasiswa(mahasiswa);

        when(nilaiValidation.validateAndGetKrs(nilaiRequest)).thenReturn(krs);
        Nilai expectedNilai = new Nilai();

        when(nilaiRepository.save(expectedNilai)).thenReturn(expectedNilai);
        assertDoesNotThrow(() -> nilaiService.createNilai(nilaiRequest, validToken));
    }

    @Test
    @DisplayName("Gagal membuat Nilai dengan token yang tidak valid")
    void createNilaiWithInvalidToken() {
        NilaiRequest nilaiRequest = new NilaiRequest();
        String invalidToken = "invalidToken";
        doThrow(new IllegalArgumentException("Invalid token")).when(nilaiValidation).validateUserRole(invalidToken);
        assertThrows(IllegalArgumentException.class, () -> nilaiService.createNilai(nilaiRequest, invalidToken));
    }

    @Test
    @DisplayName("Gagal membuat Nilai dengan request yang tidak valid")
    void createNilaiWithInvalidRequest() {
        NilaiRequest invalidNilaiRequest = new NilaiRequest();
        String validToken = "validToken";
        doThrow(new IllegalArgumentException("Invalid request")).when(nilaiValidation).validateAndGetKrs(invalidNilaiRequest);

        assertThrows(IllegalArgumentException.class, () -> nilaiService.createNilai(invalidNilaiRequest, validToken));
    }

    //Get By ID Mahasiswa
    @Test
    @DisplayName("Berhasil mendapatkan Nilai report by valid Mahasiswa ID")
    void getNilaiReportByValidMahasiswaId() {
        Integer validId = 1;
        List<NilaiReport> expectedNilaiReports = new ArrayList<>();
        when(nilaiRepository.findNilaiResponseByIdMahasiswa(validId)).thenReturn(expectedNilaiReports);

        assertDoesNotThrow(() -> nilaiService.getNilaiReportByIdMahasiswa(validId));
    }

    @Test
    @DisplayName("Gagal mendapatkan Nilai report by invalid Mahasiswa ID")
    void getNilaiReportByInvalidMahasiswaId() {
        Integer invalidId = -1;
        when(nilaiRepository.findNilaiResponseByIdMahasiswa(invalidId)).thenThrow(new IllegalArgumentException("Invalid ID"));

        assertThrows(IllegalArgumentException.class, () -> nilaiService.getNilaiReportByIdMahasiswa(invalidId));
    }

    //Update Nilai
    @Test
    @DisplayName("Berhasil update Nilai with valid request and token")
    void updateNilaiWithValidRequestAndToken() {
        Integer validId = 1;
        NilaiRequest nilaiRequest = new NilaiRequest();
        nilaiRequest.setKuis(BigDecimal.valueOf(90.0));
        nilaiRequest.setMidTest(BigDecimal.valueOf(85.0));
        nilaiRequest.setFinalTest(BigDecimal.valueOf(88.0));
        String validToken = "validToken";
        Nilai existingNilai = new Nilai();

        MataKuliah mataKuliah = new MataKuliah();
        mataKuliah.setSks(3);

        Mahasiswa mahasiswa = new Mahasiswa();

        Krs krs = new Krs();
        krs.setMataKuliah(mataKuliah);
        krs.setMahasiswa(mahasiswa);

        String invalidToken = "invalidToken";
        doThrow(new IllegalArgumentException("Invalid token")).when(nilaiValidation).validateUserRole(invalidToken);
        when(nilaiValidation.validateAndGetNilai(validId)).thenReturn(existingNilai);
        when(nilaiValidation.validateAndGetKrs(nilaiRequest)).thenReturn(krs);
        when(nilaiRepository.save(existingNilai)).thenReturn(existingNilai);

        assertDoesNotThrow(() -> nilaiService.updateNilai(validId, nilaiRequest, validToken));
    }

    @Test
    @DisplayName("Gagal update Nilai with invalid token")
    void updateNilaiWithInvalidToken() {
        Integer validId = 1;
        NilaiRequest nilaiRequest = new NilaiRequest();
        String invalidToken = "invalidToken";
        doThrow(new IllegalArgumentException("Invalid token")).when(nilaiValidation).validateUserRole(invalidToken);

        assertThrows(IllegalArgumentException.class, () -> nilaiService.updateNilai(validId, nilaiRequest, invalidToken));
    }

    @Test
    @DisplayName("Gagal update Nilai with invalid request")
    void updateNilaiWithInvalidRequest() {
        Integer validId = 1;
        NilaiRequest invalidNilaiRequest = new NilaiRequest();
        String validToken = "validToken";
        doThrow(new IllegalArgumentException("Invalid request")).when(nilaiValidation).validateAndGetKrs(invalidNilaiRequest);

        assertThrows(IllegalArgumentException.class, () -> nilaiService.updateNilai(validId, invalidNilaiRequest, validToken));
    }

    @Test
    @DisplayName("Gagal update Nilai with invalid ID")
    void updateNilaiWithInvalidId() {
        Integer invalidId = -1;
        NilaiRequest nilaiRequest = new NilaiRequest();
        String validToken = "validToken";
        doThrow(new IllegalArgumentException("Invalid ID")).when(nilaiValidation).validateAndGetNilai(invalidId);

        assertThrows(IllegalArgumentException.class, () -> nilaiService.updateNilai(invalidId, nilaiRequest, validToken));
    }

    //Delete Nilai
    @Test
    @DisplayName("Berhasil delete Nilai by valid ID")
    void deleteNilaiByValidId() {
        Integer validId = 1;
        doNothing().when(nilaiRepository).deleteById(validId);

        assertDoesNotThrow(() -> nilaiService.deleteNilaiById(validId));
    }

    @Test
    @DisplayName("Gagal delete Nilai by invalid ID")
    void deleteNilaiByInvalidId() {
        Integer invalidId = -1;
        doThrow(new IllegalArgumentException("Invalid ID")).when(nilaiRepository).deleteById(invalidId);

        assertThrows(IllegalArgumentException.class, () -> nilaiService.deleteNilaiById(invalidId));
    }
}