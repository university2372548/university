package com.miniproject.universitas.service;

import com.miniproject.universitas.dto.menu.MenuResponse;
import com.miniproject.universitas.entity.Menu;
import com.miniproject.universitas.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class MenuServiceTest {

    @InjectMocks
    private MenuService menuService;

    @Mock
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Berhasil mendapatkan semua menu")
    void getAllMenu_ValidRole_Success() {
        String role = "admin";
        Menu menu1 = new Menu();
        menu1.setRole(role);
        Menu menu2 = new Menu();
        menu2.setRole(role);
        List<Menu> menuList = new ArrayList<>();
        menuList.add(menu1);
        menuList.add(menu2);

        when(menuRepository.findAll()).thenReturn(menuList);

        List<MenuResponse> responseList = menuService.getAllMenu(role);

        assertEquals(2, responseList.size());
    }

    @Test
    @DisplayName("Gagal mendapatkan semua menu")
    void getAllMenu_InvalidRole_EmptyList() {
        String role = "invalidRole";
        Menu menu1 = new Menu();
        menu1.setRole("admin");
        Menu menu2 = new Menu();
        menu2.setRole("admin");
        List<Menu> menuList = new ArrayList<>();
        menuList.add(menu1);
        menuList.add(menu2);

        when(menuRepository.findAll()).thenReturn(menuList);

        List<MenuResponse> responseList = menuService.getAllMenu(role);

        assertEquals(0, responseList.size());
    }
}
