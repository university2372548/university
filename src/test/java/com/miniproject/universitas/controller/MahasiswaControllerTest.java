package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.mahasiswa.MahasiswaRequest;
import com.miniproject.universitas.dto.mahasiswa.MahasiswaResponse;
import com.miniproject.universitas.service.MahasiswaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

class MahasiswaControllerTest {

    @InjectMocks
    private MahasiswaController mahasiswaController;

    @Mock
    private MahasiswaService mahasiswaService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    //Post Test
    @Test
    @DisplayName("Berhasil membuat mahasiswa")
    void createMahasiswaReturnsOkWhenSuccessful() {
        MahasiswaRequest request = new MahasiswaRequest();
        MahasiswaResponse expectedResponse = new MahasiswaResponse();
        Mockito.when(mahasiswaService.createMahasiswa(any(MahasiswaRequest.class), eq("validToken"))).thenReturn(expectedResponse);

        ResponseEntity<MahasiswaResponse> responseEntity = mahasiswaController.createMahasiswa(request, "validToken");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal membuat mahasiswa ketika service error")
    void createMahasiswaReturnsBadRequestWhenServiceThrowsException() {
        MahasiswaRequest request = new MahasiswaRequest();
        Mockito.when(mahasiswaService.createMahasiswa(any(MahasiswaRequest.class), eq("invalidToken"))).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> mahasiswaController.createMahasiswa(request, "invalidToken"));
    }

    //Get Test
    @Test
    @DisplayName("Berhasil mendapatkan all mahasiswa ketika service sukses")
    void getAllMahasiswaReturnsOkWhenSuccessful() {
        List<MahasiswaResponse> expectedResponse = java.util.Arrays.asList(new MahasiswaResponse(), new MahasiswaResponse());
        Mockito.when(mahasiswaService.getMahasiswaAll(any(String.class))).thenReturn(expectedResponse);

        ResponseEntity<List<MahasiswaResponse>> responseEntity = mahasiswaController.getAllMahasiswa("validToken");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan all mahasiswa ketika service bad request")
    void getAllMahasiswaReturnsBadRequestWhenServiceThrowsException() {
        Mockito.when(mahasiswaService.getMahasiswaAll(any(String.class))).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> mahasiswaController.getAllMahasiswa("invalidToken"));
    }

    //Get By Id Test
    @Test
    @DisplayName("Berhasil mendapatkan mahasiswa berdasarkan id ketika service sukses")
    void getMahasiswaByIdReturnsOkWhenSuccessful() {
        MahasiswaResponse expectedResponse = new MahasiswaResponse();
        Mockito.when(mahasiswaService.getMahasiswaById(1, "validToken")).thenReturn(expectedResponse);
        ResponseEntity<MahasiswaResponse> responseEntity = mahasiswaController.getMahasiswaById(1, "validToken");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan mahasiswa berdasarkan id ketika service error")
    void getMahasiswaByIdReturnsBadRequestWhenServiceThrowsException() {
        Mockito.when(mahasiswaService.getMahasiswaById(1, "invalidToken")).thenThrow(IllegalArgumentException.class);
        assertThrows(IllegalArgumentException.class, () -> mahasiswaController.getMahasiswaById(1, "invalidToken"));
    }

    //Put Test
    @Test
    @DisplayName("Berhasil update mahasiswa ketika service sukses")
    void updateMahasiswaReturnsOkWhenSuccessful() {
        MahasiswaRequest request = new MahasiswaRequest();
        MahasiswaResponse expectedResponse = new MahasiswaResponse();
        Mockito.when(mahasiswaService.updateMahasiswa(eq(1), any(MahasiswaRequest.class), eq("validToken"))).thenReturn(expectedResponse);

        ResponseEntity<MahasiswaResponse> responseEntity = mahasiswaController.updateMahasiswa(1, request, "validToken");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal update mahasiswa ketika service error")
    void updateMahasiswaReturnsBadRequestWhenServiceThrowsException() {
        MahasiswaRequest request = new MahasiswaRequest();
        Mockito.when(mahasiswaService.updateMahasiswa(eq(1), any(MahasiswaRequest.class), eq("invalidToken"))).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> mahasiswaController.updateMahasiswa(1, request, "invalidToken"));
    }

    @Test
    @DisplayName("Berhasil delete mahasiswa ketika service sukses")
    void deleteMahasiswaReturnsOkWhenSuccessful() {
        Mockito.when(mahasiswaService.deleteMahasiswa(1, "validToken")).thenReturn("Mahasiswa deleted successfully");
        ResponseEntity<String> responseEntity = mahasiswaController.deleteMahasiswa(1, "validToken");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Mahasiswa deleted successfully", responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal delete mahasiswa ketika service error")
    void deleteMahasiswaReturnsBadRequestWhenServiceThrowsException() {
        Mockito.doThrow(IllegalArgumentException.class).when(mahasiswaService).deleteMahasiswa(1, "invalidToken");
        assertThrows(IllegalArgumentException.class, () -> mahasiswaController.deleteMahasiswa(1, "invalidToken"));
    }
}