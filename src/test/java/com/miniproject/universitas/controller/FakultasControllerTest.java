package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.fakultas.FakultasRequest;
import com.miniproject.universitas.dto.fakultas.FakultasResponse;
import com.miniproject.universitas.service.FakultasService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class FakultasControllerTest {

    @InjectMocks
    FakultasController fakultasController;

    @Mock
    FakultasService fakultasService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    //Create Fakultas
    @Test
    @DisplayName("Berhasil membuat fakultas ketika Return OK")
    void createFakultasReturnsCreatedStatus() {
        FakultasRequest request = new FakultasRequest();
        FakultasResponse response = new FakultasResponse();
        when(fakultasService.createFakultas(request, "token")).thenReturn(response);

        ResponseEntity<FakultasResponse> result = fakultasController.createFakultas(request, "token");

        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertEquals(response, result.getBody());
    }

    @Test
    @DisplayName("Gagal membuat fakultas ketika Return BadRequest")
    void createFakultasReturnsErrorWhenServiceFails() {
        FakultasRequest request = new FakultasRequest();
        when(fakultasService.createFakultas(request, "token")).thenReturn(null);

        ResponseEntity<FakultasResponse> result = fakultasController.createFakultas(request, "token");

        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertNull(result.getBody());
    }

    //Get All Fakultas
    @Test
    @DisplayName("Berhasil mendapatkan semua fakultas ketika Return OK")
    void getAllFakultasReturnsOkStatus() {
        List<FakultasResponse> responseList = Collections.singletonList(new FakultasResponse());
        when(fakultasService.getAllFakultas("token")).thenReturn(responseList);

        ResponseEntity<List<FakultasResponse>> result = fakultasController.getAllFakultas("token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(responseList, result.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan semua fakultas ketika Return BadRequest")
    void getAllFakultasReturnsEmptyListWhenNoFakultas() {
        when(fakultasService.getAllFakultas("token")).thenReturn(Collections.emptyList());

        ResponseEntity<List<FakultasResponse>> result = fakultasController.getAllFakultas("token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(Collections.emptyList(), result.getBody());
    }

    //Get Fakultas By Id
    @Test
    @DisplayName("Berhasil mendapatkan fakultas by id ketika Return OK")
    void getFakultasByIdReturnsOkStatus() {
        Integer id = 1;
        FakultasResponse response = new FakultasResponse();
        when(fakultasService.getFakultasById(id, "token")).thenReturn(response);

        ResponseEntity<FakultasResponse> result = fakultasController.getFakultasById(id, "token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(response, result.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan fakultas by id ketika Return BadRequest")
    void getFakultasByIdReturnsNotFoundWhenIdDoesNotExist() {
        Integer id = 1;
        when(fakultasService.getFakultasById(id, "token")).thenReturn(null);

        ResponseEntity<FakultasResponse> result = fakultasController.getFakultasById(id, "token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertNull(result.getBody());
    }

    //Update Fakultas By Id
    @Test
    @DisplayName("Berhasil update fakultas by id ketika Return OK")
    void updateFakultasByIdReturnsOkStatus() {
        Integer id = 1;
        FakultasRequest request = new FakultasRequest();
        FakultasResponse response = new FakultasResponse();
        when(fakultasService.updateFakultasById(id, request, "token")).thenReturn(response);

        ResponseEntity<FakultasResponse> result = fakultasController.updateFakultasById(id, request, "token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(response, result.getBody());
    }

    @Test
    @DisplayName("Gagal update fakultas by id ketika Return BadRequest")
    void updateFakultasByIdReturnsNotFoundWhenIdDoesNotExist() {
        Integer id = 1;
        FakultasRequest request = new FakultasRequest();
        when(fakultasService.updateFakultasById(id, request, "token")).thenReturn(null);

        ResponseEntity<FakultasResponse> result = fakultasController.updateFakultasById(id, request, "token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertNull(result.getBody());
    }

    //Delete Fakultas By Id
    @Test
    @DisplayName("Berhasil delete fakultas by id ketika Return OK")
    void deleteFakultasByIdReturnsOkStatus() {
        Integer id = 1;
        when(fakultasService.deleteFakultasById(id, "token")).thenReturn("Fakultas deleted successfully");

        ResponseEntity<String> result = fakultasController.deleteFakultasById(id, "token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Fakultas deleted successfully", result.getBody());
    }

    @Test
    @DisplayName("Gagal delete fakultas by id ketika Return BadRequest")
    void deleteFakultasByIdThrowsExceptionWhenIdDoesNotExist() {
        Integer id = 1;
        doThrow(new RuntimeException()).when(fakultasService).deleteFakultasById(id, "token");

        assertThrows(RuntimeException.class, () -> fakultasController.deleteFakultasById(id, "token"));
    }
}
