package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.menu.MenuResponse;
import com.miniproject.universitas.service.MenuService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class MenuControllerTest {

    @InjectMocks
    private MenuController menuController;

    @Mock
    private MenuService menuService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Berhasil menampilkan semua menu ketika Request OK")
    void getAllMenuReturnsOkWhenSuccessful() {
        List<MenuResponse> expectedResponse = java.util.Arrays.asList(
                new MenuResponse(1, "Menu1", "url1", "role1"),
                new MenuResponse(2, "Menu2", "url2", "role2")
        );
        Mockito.when(menuService.getAllMenu(any(String.class))).thenReturn(expectedResponse);

        ResponseEntity<List<MenuResponse>> responseEntity = menuController.getAllMenu("validRole");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal menampilkan semua menu ketika Request Bad Request")
    void getAllMenuReturnsBadRequestWhenServiceThrowsException() {
        Mockito.when(menuService.getAllMenu(any(String.class))).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> menuController.getAllMenu("invalidRole"));
    }
}