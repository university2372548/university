package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.dosen.DosenResponse;
import com.miniproject.universitas.service.DosenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

class DosenControllerTest {

    @InjectMocks
    private DosenController dosenController;

    @Mock
    private DosenService dosenService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    //GetByID
    @Test
    @DisplayName("Berhasil mendapatkan dosen ketika Return OK")
    void getDosenByIdReturnsCorrectDosen() {
        Integer id = 1;
        DosenResponse dosenResponse = new DosenResponse();
        when(dosenService.getDosenById(id, "token")).thenReturn(dosenResponse);

        ResponseEntity<DosenResponse> response = dosenController.getDosenById(id, "token");

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(dosenResponse, response.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan dosen ketika Return NotFound")
    void getDosenByIdReturnsNotFoundForInvalidId() {
        Integer id = 1;
        when(dosenService.getDosenById(id, "token")).thenReturn(null);

        ResponseEntity<DosenResponse> response = dosenController.getDosenById(id, "token");

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNull(response.getBody());
    }


    //GetAll
    @Test
    @DisplayName("Berhasil mendapatkan semua dosen ketika Return OK")
    void getDosenAllReturnsCorrectDosenList() {

        List<DosenResponse> dosenResponses = Collections.singletonList(new DosenResponse());
        when(dosenService.getDosenAll("token")).thenReturn(dosenResponses);

        ResponseEntity<List<DosenResponse>> response = dosenController.getDosenAll("token");

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(dosenResponses, response.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan semua dosen ketika Return NotFound")
    void getDosenAllReturnsNotFoundWhenNoDosen() {

        when(dosenService.getDosenAll("token")).thenReturn(Collections.emptyList());
        ResponseEntity<List<DosenResponse>> response = dosenController.getDosenAll("token");

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(Collections.emptyList(), response.getBody());
    }
}
