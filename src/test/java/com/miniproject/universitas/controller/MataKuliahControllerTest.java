package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.matakuliah.MataKuliahRequest;
import com.miniproject.universitas.dto.matakuliah.MataKuliahResponse;
import com.miniproject.universitas.service.MataKuliahService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

class MataKuliahControllerTest {

    @InjectMocks
    private MataKuliahController mataKuliahController;

    @Mock
    private MataKuliahService mataKuliahService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    //Post Test
    @Test
    @DisplayName("Berhasil membuat mata kuliah baru")
    void createMataKuliahReturnsCreatedWhenSuccessful() {
        MataKuliahRequest request = new MataKuliahRequest();
        MataKuliahResponse expectedResponse = new MataKuliahResponse(1, "Example Mata Kuliah", 3, "Example Description");
        Mockito.when(mataKuliahService.createMataKuliah(any(MataKuliahRequest.class), eq("validToken"))).thenReturn(expectedResponse);

        ResponseEntity<MataKuliahResponse> responseEntity = mataKuliahController.createMataKuliah(request, "validToken");

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal membuat mata kuliah baru karena BadRequest")
    void createMataKuliahReturnsBadRequestWhenServiceThrowsException() {
        MataKuliahRequest request = new MataKuliahRequest();
        Mockito.when(mataKuliahService.createMataKuliah(any(MataKuliahRequest.class), eq("invalidToken"))).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> mataKuliahController.createMataKuliah(request, "invalidToken"));
    }

    //Get All Test
    @Test
    @DisplayName("Berhasil mendapatkan semua mata kuliah")
    void getMataKuliahAllReturnsOkWhenSuccessful() {
        List<MataKuliahResponse> expectedResponse = java.util.Arrays.asList(new MataKuliahResponse(1, "Example Mata Kuliah", 3, "Example Description"), new MataKuliahResponse(2, "Example Mata Kuliah 2", 3, "Example Description 2"));
        Mockito.when(mataKuliahService.getMataKuliahAll()).thenReturn(expectedResponse);

        ResponseEntity<List<MataKuliahResponse>> responseEntity = mataKuliahController.getMataKuliahAll();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan semua mata kuliah karena BadRequest")
    void getMataKuliahAllReturnsBadRequestWhenServiceThrowsException() {
        Mockito.when(mataKuliahService.getMataKuliahAll()).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> mataKuliahController.getMataKuliahAll());
    }

    //Get By Nama Mata Kuliah Test
    @Test
    @DisplayName("Berhasil mendapatkan mata kuliah berdasarkan nama mata kuliah")
    void getMataKuliahByNamaMataKuliahReturnsOkWhenSuccessful() {
        List<MataKuliahResponse> expectedResponse = java.util.Arrays.asList(new MataKuliahResponse(1, "Example Mata Kuliah", 3, "Example Description"), new MataKuliahResponse(2, "Example Mata Kuliah 2", 3, "Example Description 2"));
        Mockito.when(mataKuliahService.getMataKuliahByNamaMataKuliah("validMataKuliah", "validToken")).thenReturn(expectedResponse);
        ResponseEntity<List<MataKuliahResponse>> responseEntity = mataKuliahController.getMataKuliahByNamaMataKuliah("validMataKuliah", "validToken");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gaagal mendapatkan mata kuliah berdasarkan nama mata kuliah karena BadRequest")
    void getMataKuliahByNamaMataKuliahReturnsBadRequestWhenServiceThrowsException() {
        Mockito.when(mataKuliahService.getMataKuliahByNamaMataKuliah("invalidMataKuliah", "invalidToken")).thenThrow(IllegalArgumentException.class);
        assertThrows(IllegalArgumentException.class, () -> mataKuliahController.getMataKuliahByNamaMataKuliah("invalidMataKuliah", "invalidToken"));
    }

    @Test
    @DisplayName("Berhasil mendapatkan mata kuliah ketika Return OK")
    void updateMataKuliahReturnsOkWhenSuccessful() {
        MataKuliahRequest request = new MataKuliahRequest();
        MataKuliahResponse expectedResponse = new MataKuliahResponse(1, "Example Mata Kuliah", 3, "Example Description");
        Mockito.when(mataKuliahService.updateMataKuliah(any(MataKuliahRequest.class), eq("validToken"), eq(1))).thenReturn(expectedResponse);

        ResponseEntity<MataKuliahResponse> responseEntity = mataKuliahController.updateMataKuliah(1, request, "validToken");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan mata kuliah ketika Return BadRequest")
    void updateMataKuliahReturnsBadRequestWhenServiceThrowsException() {
        MataKuliahRequest request = new MataKuliahRequest();
        Mockito.when(mataKuliahService.updateMataKuliah(any(MataKuliahRequest.class), eq("invalidToken"), eq(1))).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> mataKuliahController.updateMataKuliah(1, request, "invalidToken"));
    }

    @Test
    @DisplayName("Berhasil menghapus mata kuliah ketika Return OK")
    void deleteMataKuliahReturnsOkWhenSuccessful() {
        Mockito.when(mataKuliahService.deleteMataKuliah(1)).thenReturn("Mata Kuliah deleted successfully");
        ResponseEntity<String> responseEntity = mataKuliahController.deleteMataKuliah(1);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Mata Kuliah deleted successfully", responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal menghapus mata kuliah ketika Return BadRequest")
    void deleteMataKuliahReturnsBadRequestWhenServiceThrowsException() {
        Mockito.doThrow(IllegalArgumentException.class).when(mataKuliahService).deleteMataKuliah(1);
        assertThrows(IllegalArgumentException.class, () -> mataKuliahController.deleteMataKuliah(1));
    }
}