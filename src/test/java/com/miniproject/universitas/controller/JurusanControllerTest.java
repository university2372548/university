package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.jurusan.JurusanRequest;
import com.miniproject.universitas.dto.jurusan.JurusanResponse;
import com.miniproject.universitas.entity.Jurusan;
import com.miniproject.universitas.repository.JurusanRepository;
import com.miniproject.universitas.service.JurusanService;
import com.miniproject.universitas.validation.JurusanValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class JurusanControllerTest {

    @InjectMocks
    JurusanController jurusanController;

    @Mock
    JurusanService jurusanService;

    @Mock
    JurusanValidation jurusanValidation;

    @Mock
    JurusanRepository jurusanRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    //Create Jurusan
    @Test
    @DisplayName("Berhasil membuat jurusan ketika Return OK")
    void createJurusanReturnsCreatedStatus() {
        JurusanRequest request = new JurusanRequest();
        JurusanResponse response = new JurusanResponse();
        when(jurusanService.createJurusan(request, "token")).thenReturn(response);

        ResponseEntity<JurusanResponse> result = jurusanController.createJurusan(request, "token");

        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertEquals(response, result.getBody());
    }

    @Test
    @DisplayName("Gagal membuat jurusan ketika Return BadRequest")
    void createJurusanReturnsErrorWhenServiceFails() {
        JurusanRequest request = new JurusanRequest();
        when(jurusanService.createJurusan(request, "token")).thenReturn(null);

        ResponseEntity<JurusanResponse> result = jurusanController.createJurusan(request, "token");

        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertNull(result.getBody());
    }

    //Get All Jurusan

    @Test
    @DisplayName("getAllJurusan returns JurusanResponse list when Jurusan list is not empty")
    void getAllJurusan_ReturnsJurusanResponseList_WhenJurusanListIsNotEmpty() {
        String token = "validToken";

        JurusanResponse jurusanResponse1 = new JurusanResponse();
        JurusanResponse jurusanResponse2 = new JurusanResponse();
        List<JurusanResponse> jurusanResponseList = java.util.Arrays.asList(jurusanResponse1, jurusanResponse2);

        when(jurusanService.getAllJurusan(token)).thenReturn(jurusanResponseList);

        ResponseEntity<List<JurusanResponse>> result = jurusanController.getAllJurusan(token);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(jurusanResponseList, result.getBody());
    }

    @Test
    @DisplayName("getAllJurusan throws exception when invalid token provided")
    void getAllJurusan_InvalidToken_ExceptionThrown() {
        String token = "invalidToken";

        when(jurusanService.getAllJurusan(token)).thenThrow(new IllegalArgumentException("Invalid token"));

        assertThrows(IllegalArgumentException.class, () -> jurusanController.getAllJurusan(token));
    }

    //Get Jurusan By Id
    @Test
    @DisplayName("Berhasil mendapatkan jurusan by id ketika Return OK")
    void getJurusanById_ReturnsJurusanResponse_WhenJurusanExists() {
        Integer id = 1;
        String token = "validToken";
        JurusanResponse response = new JurusanResponse();

        when(jurusanService.getJurusanById(id, token)).thenReturn(response);

        ResponseEntity<JurusanResponse> result = jurusanController.getJurusanById(id, token);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(response, result.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan jurusan by id ketika Return BadRequest")
    void getJurusanById_InvalidToken_ExceptionThrown() {
        Integer id = 1;
        String token = "invalidToken";

        when(jurusanService.getJurusanById(id, token)).thenThrow(new IllegalArgumentException("Invalid token"));

        assertThrows(IllegalArgumentException.class, () -> jurusanController.getJurusanById(id, token));
    }

    //Update Jurusan By Id
    @Test
    @DisplayName("Berhasil mengupdate jurusan ketika Return OK")
    void updateJurusanByIdReturnsOkStatus() {
        Integer id = 1;
        JurusanRequest request = new JurusanRequest();
        JurusanResponse response = new JurusanResponse();
        when(jurusanService.updateJurusanById(id, request, "token")).thenReturn(response);

        ResponseEntity<JurusanResponse> result = jurusanController.updateJurusanById(id, request, "token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(response, result.getBody());
    }

    @Test
    @DisplayName("Gagal mengupdate jurusan ketika Return BadRequest")
    void updateJurusanByIdReturnsNotFoundWhenIdDoesNotExist() {
        Integer id = 1;
        JurusanRequest request = new JurusanRequest();
        when(jurusanService.updateJurusanById(id, request, "token")).thenReturn(null);

        ResponseEntity<JurusanResponse> result = jurusanController.updateJurusanById(id, request, "token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertNull(result.getBody());
    }

    //Delete Jurusan By Id
    @Test
    @DisplayName("Berhasil menghapus jurusan ketika Return OK")
    void deleteJurusanByIdReturnsOkStatus() {
        Integer id = 1;
        when(jurusanService.deleteJurusanById(id, "token")).thenReturn("Jurusan deleted successfully");

        ResponseEntity<String> result = jurusanController.deleteJurusanById(id, "token");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Jurusan deleted successfully", result.getBody());
    }

    @Test
    @DisplayName("Gagal menghapus jurusan ketika Return BadRequest")
    void deleteJurusanByIdThrowsExceptionWhenIdDoesNotExist() {
        Integer id = 1;
        doThrow(new RuntimeException()).when(jurusanService).deleteJurusanById(id, "token");

        assertThrows(RuntimeException.class, () -> jurusanController.deleteJurusanById(id, "token"));
    }
}
