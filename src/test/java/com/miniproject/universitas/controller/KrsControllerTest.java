package com.miniproject.universitas.controller;

import com.lowagie.text.DocumentException;
import com.miniproject.universitas.dto.krs.KrsDetailNilai;
import com.miniproject.universitas.dto.krs.KrsReport;
import com.miniproject.universitas.dto.krs.KrsRequest;
import com.miniproject.universitas.dto.krs.KrsResponse;
import com.miniproject.universitas.service.KrsService;
import com.miniproject.universitas.util.PdfGeneratorKrs;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class KrsControllerTest {

    @InjectMocks
    KrsController krsController;

    @Mock
    KrsService krsService;

    @Mock
    PdfGeneratorKrs pdfGeneratorKrs;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    //Post Test
    @Test
    @DisplayName("Berhasil membuat KRS dengan request dan token yang valid")
    void createKrsWithValidRequestAndToken() {
        KrsRequest krsRequest = new KrsRequest();
        String validToken = "validToken";
        KrsResponse expectedResponse = new KrsResponse();
        when(krsService.createKrs(krsRequest, validToken)).thenReturn(expectedResponse);

        ResponseEntity<KrsResponse> responseEntity = krsController.createKrs(krsRequest, validToken);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal membuat KRS dengan request yang valid dan token yang tidak valid")
    void createKrsWithInvalidToken() {
        KrsRequest krsRequest = new KrsRequest();
        String invalidToken = "invalidToken";
        when(krsService.createKrs(krsRequest, invalidToken)).thenThrow(new IllegalArgumentException("Invalid token"));

        assertThrows(IllegalArgumentException.class, () -> krsController.createKrs(krsRequest, invalidToken));
    }

    //Get Test
    @Test
    @DisplayName("Berhasil mendapatkan KRS dengan ID yang valid")
    void getKrsByIdWithValidId() {
        Integer validId = 1;
        KrsResponse expectedResponse = new KrsResponse();
        when(krsService.getKrsById(validId)).thenReturn(expectedResponse);

        ResponseEntity<KrsResponse> responseEntity = krsController.getKrsById(validId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan KRS dengan ID yang tidak valid")
    void getKrsByIdWithInvalidId() {
        Integer invalidId = -1;
        when(krsService.getKrsById(invalidId)).thenThrow(new IllegalArgumentException("Invalid ID"));

        assertThrows(IllegalArgumentException.class, () -> krsController.getKrsById(invalidId));
    }

    //Put Test
    @Test
    @DisplayName("Berhasil update KRS dengan request dan token yang valid")
    void updateKrsWithValidRequestAndToken() {
        Integer validId = 1;
        KrsRequest krsRequest = new KrsRequest();
        String validToken = "validToken";
        KrsResponse expectedResponse = new KrsResponse();
        when(krsService.updateKrs(validId, krsRequest, validToken)).thenReturn(expectedResponse);

        ResponseEntity<KrsResponse> responseEntity = krsController.updateKrs(validId, krsRequest, validToken);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal update KRS dengan token yang tidak valid")
    void updateKrsWithInvalidToken() {
        Integer validId = 1;
        KrsRequest krsRequest = new KrsRequest();
        String invalidToken = "invalidToken";
        when(krsService.updateKrs(validId, krsRequest, invalidToken)).thenThrow(new IllegalArgumentException("Invalid token"));

        assertThrows(IllegalArgumentException.class, () -> krsController.updateKrs(validId, krsRequest, invalidToken));
    }

    @Test
    @DisplayName("Gagal update KRS dengan request yang tidak valid")
    void updateKrsWithInvalidRequest() {
        Integer validId = 1;
        KrsRequest invalidKrsRequest = new KrsRequest();
        String validToken = "validToken";
        when(krsService.updateKrs(validId, invalidKrsRequest, validToken)).thenThrow(new IllegalArgumentException("Invalid request"));

        assertThrows(IllegalArgumentException.class, () -> krsController.updateKrs(validId, invalidKrsRequest, validToken));
    }

    @Test
    @DisplayName("Gagal update KRS dengan ID yang tidak valid")
    void updateKrsWithInvalidId() {
        Integer invalidId = -1;
        KrsRequest krsRequest = new KrsRequest();
        String validToken = "validToken";
        when(krsService.updateKrs(invalidId, krsRequest, validToken)).thenThrow(new IllegalArgumentException("Invalid ID"));

        assertThrows(IllegalArgumentException.class, () -> krsController.updateKrs(invalidId, krsRequest, validToken));
    }

    //Delete Test
    @Test
    @DisplayName("Berhasil delete KRS dengan ID dan token yang valid")
    void deleteKrsByValidId() {
        Integer validId = 1;
        String validToken = "validToken";
        when(krsService.deleteKrsById(validId, validToken)).thenReturn("anyValue");

        assertDoesNotThrow(() -> krsController.deleteById(validId, validToken));
    }

    @Test
    @DisplayName("Gagal delete KRS dengan ID yang tidak valid")
    void deleteKrsByInvalidId() {
        Integer invalidId = -1;
        String validToken = "validToken";
        doThrow(new IllegalArgumentException("Invalid ID")).when(krsService).deleteKrsById(invalidId, validToken);

        assertThrows(IllegalArgumentException.class, () -> krsController.deleteById(invalidId, validToken));
    }

    @Test
    @DisplayName("Gagal delete KRS dengan token yang tidak valid")
    void deleteKrsByValidIdAndInvalidToken() {
        Integer validId = 1;
        String invalidToken = "invalidToken";
        doThrow(new IllegalArgumentException("Invalid token")).when(krsService).deleteKrsById(validId, invalidToken);

        assertThrows(IllegalArgumentException.class, () -> krsController.deleteById(validId, invalidToken));
    }

    //Generate PDF Test
    @Test
    @DisplayName("Berhasil generate PDF file untuk ID mahasiswa yang valid")
    void generatePdfFileForValidId() throws IOException, DocumentException {
        Integer validId = 1;
        HttpServletResponse response = mock(HttpServletResponse.class);
        List<KrsReport> krsReports = new ArrayList<>();
        when(krsService.getKrsReportByIdMahasiswa(validId)).thenReturn(krsReports);

        assertDoesNotThrow(() -> krsController.generatePdfFile(response, validId));
    }

    @Test
    @DisplayName("Gagal generate PDF file untuk ID mahasiswa yang tidak valid")
    void generatePdfFileForInvalidId() throws IOException, DocumentException {
        Integer invalidId = -1;
        HttpServletResponse response = mock(HttpServletResponse.class);
        when(krsService.getKrsReportByIdMahasiswa(invalidId)).thenThrow(new IllegalArgumentException("Invalid ID"));

        assertThrows(IllegalArgumentException.class, () -> krsController.generatePdfFile(response, invalidId));
    }

    @Test
    @DisplayName("getAllFakultas returns correct KrsDetailNilai list when KrsDetailNilai exist")
    void getAllFakultasReturnsCorrectListWhenKrsDetailNilaiExist() {
        KrsDetailNilai krsDetailNilai1 = new KrsDetailNilai(1, "string2", "string3", "string1", 1);
        KrsDetailNilai krsDetailNilai2 = new KrsDetailNilai(2, "string5", "string6", "string4", 2);
        List<KrsDetailNilai> expectedList = Arrays.asList(krsDetailNilai1, krsDetailNilai2);

        when(krsService.getAllKrs()).thenReturn(expectedList);

        ResponseEntity<List<KrsDetailNilai>> actualList = krsController.getAllFakultas();

        assertEquals(HttpStatus.OK, actualList.getStatusCode());
        assertEquals(expectedList, actualList.getBody());
    }

    @Test
    @DisplayName("getAllFakultas returns empty list when no KrsDetailNilai")
    void getAllFakultasReturnsEmptyListWhenNoKrsDetailNilai() {
        when(krsService.getAllKrs()).thenReturn(Collections.emptyList());

        ResponseEntity<List<KrsDetailNilai>> actualList = krsController.getAllFakultas();

        assertEquals(HttpStatus.OK, actualList.getStatusCode());
        assertEquals(Collections.emptyList(), actualList.getBody());
    }

    @Test
    @DisplayName("Berhasil mendapatkan KRS dengan NID yang valid")
    void getAllKrsByNidReturnsCorrectListWhenKrsDetailNilaiWithMatchingNidExist() {
        String nid = "validNid";
        KrsDetailNilai krsDetailNilai1 = new KrsDetailNilai(1, "string1", nid, "string3", 1);
        KrsDetailNilai krsDetailNilai2 = new KrsDetailNilai(2, "string4", nid, "string6", 2);
        List<KrsDetailNilai> expectedList = Arrays.asList(krsDetailNilai1, krsDetailNilai2);

        when(krsService.getAllKrsByNid(nid)).thenReturn(expectedList);

        ResponseEntity<List<KrsDetailNilai>> actualList = krsController.getAllKrsByNid(nid);

        assertEquals(HttpStatus.OK, actualList.getStatusCode());
        assertEquals(expectedList, actualList.getBody());
    }

    @Test
    @DisplayName("Gagal mendapatkan all KRS dengan NID yang valid")
    void getAllKrsByNidReturnsEmptyListWhenNoKrsDetailNilaiWithMatchingNid() {
        String nid = "validNid";
        KrsDetailNilai krsDetailNilai1 = new KrsDetailNilai(1, "string1", "differentNid", "string3", 1);
        KrsDetailNilai krsDetailNilai2 = new KrsDetailNilai(2, "string4", "differentNid", "string6", 2);

        when(krsService.getAllKrsByNid(nid)).thenReturn(Collections.emptyList());

        ResponseEntity<List<KrsDetailNilai>> actualList = krsController.getAllKrsByNid(nid);

        assertEquals(HttpStatus.OK, actualList.getStatusCode());
        assertTrue(actualList.getBody().isEmpty());
    }

    @Test
    @DisplayName("Gagal mendapatkan all KRS dengan NID yang tidak valid")
    void getAllKrsByNidReturnsEmptyListWhenKrsDetailNilaiListIsEmpty() {
        String nid = "validNid";

        when(krsService.getAllKrsByNid(nid)).thenReturn(Collections.emptyList());

        ResponseEntity<List<KrsDetailNilai>> actualList = krsController.getAllKrsByNid(nid);

        assertEquals(HttpStatus.OK, actualList.getStatusCode());
        assertTrue(actualList.getBody().isEmpty());
    }
}