package com.miniproject.universitas.controller;

import com.lowagie.text.DocumentException;
import com.miniproject.universitas.dto.nilai.NilaiReport;
import com.miniproject.universitas.dto.nilai.NilaiRequest;
import com.miniproject.universitas.dto.nilai.NilaiResponse;
import com.miniproject.universitas.service.NilaiService;
import com.miniproject.universitas.util.PdfGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

class NilaiControllerTest {

    @InjectMocks
    NilaiController nilaiController;

    @Mock
    NilaiService nilaiService;

    @Mock
    PdfGenerator pdfGenerator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        nilaiController = new NilaiController(nilaiService, pdfGenerator);
    }

    //Post Test
    @Test
    @DisplayName("Berhasil membuat nilai baru ketika Return OK")
    void createNilaiReturnsOkWhenSuccessful() {
        NilaiRequest request = new NilaiRequest();
        NilaiResponse expectedResponse = new NilaiResponse();
        Mockito.when(nilaiService.createNilai(any(NilaiRequest.class), eq("validToken"))).thenReturn(expectedResponse);

        ResponseEntity<NilaiResponse> responseEntity = nilaiController.createNilai(request, "validToken");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal membuat nilai baru ketika Return BadRequest")
    void createNilaiReturnsBadRequestWhenServiceThrowsException() {
        NilaiRequest request = new NilaiRequest();
        Mockito.when(nilaiService.createNilai(any(NilaiRequest.class), eq("invalidToken"))).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> nilaiController.createNilai(request, "invalidToken"));
    }

    //Get Test
    @Test
    @DisplayName("Berhasil mengambil nilai report ketika Return OK")
    void getNilaiReportReturnsOkWhenSuccessful() {
        List<NilaiReport> expectedResponse = java.util.Arrays.asList(
                new NilaiReport(1, 1, "nim1", "nama1", "jurusan1", "fakultas1", "matakuliah1", 3, BigDecimal.valueOf(80), BigDecimal.valueOf(85.0), BigDecimal.valueOf(90.0), BigDecimal.valueOf(88.0), 4, "A"),
                new NilaiReport(2, 2, "nim2", "nama2", "jurusan2", "fakultas2", "matakuliah2", 3, BigDecimal.valueOf(75.0), BigDecimal.valueOf(80.0), BigDecimal.valueOf(85.0), BigDecimal.valueOf(80.0), 3, "B")
        );
        Mockito.when(nilaiService.getNilaiReportByIdMahasiswa(any(Integer.class))).thenReturn(expectedResponse);

        ResponseEntity<List<NilaiReport>> responseEntity = nilaiController.getNilaiReport(1);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal mengambil nilai report ketika Return BadRequest")
    void getNilaiReportReturnsBadRequestWhenServiceThrowsException() {
        Mockito.when(nilaiService.getNilaiReportByIdMahasiswa(any(Integer.class))).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> nilaiController.getNilaiReport(1));
    }

    //Get PDF Test
    @Test
    @DisplayName("Berhasil generate pdf ketika Return OK")
    void generatePdfFileReturnsPdfWhenSuccessful() throws DocumentException, IOException {
        new NilaiReport(1, 1, "nim1", "nama1", "jurusan1", "fakultas1", "matakuliah1", 3, BigDecimal.valueOf(80.0), BigDecimal.valueOf(85.0), BigDecimal.valueOf(90.0), BigDecimal.valueOf(88.0), 4, "A");
        MockHttpServletResponse response = new MockHttpServletResponse();
        nilaiController.generatePdfFile(response, 1);

        assertEquals("application/pdf", response.getContentType());
    }

    @Test
    @DisplayName("Gagal generate pdf ketika Return BadRequest")
    void generatePdfFileThrowsDocumentExceptionWhenServiceThrowsException() throws DocumentException, IOException {
        Mockito.when(nilaiService.getNilaiReportByIdMahasiswa(any(Integer.class))).thenThrow(DocumentException.class);

        MockHttpServletResponse response = new MockHttpServletResponse();
        assertThrows(DocumentException.class, () -> nilaiController.generatePdfFile(response, 1));
    }

    //Put Test
    @Test
    @DisplayName("Berhasil Update nilai ketika Return OK")
    void updateNilaiReturnsOkWhenSuccessful() {
        NilaiRequest request = new NilaiRequest();
        NilaiResponse expectedResponse = new NilaiResponse();
        Mockito.when(nilaiService.updateNilai(eq(1), any(NilaiRequest.class), eq("validToken"))).thenReturn(expectedResponse);

        ResponseEntity<NilaiResponse> responseEntity = nilaiController.updateNilai(1, request, "validToken");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal Update nilai ketika Return BadRequest")
    void updateNilaiReturnsBadRequestWhenServiceThrowsException() {
        NilaiRequest request = new NilaiRequest();
        Mockito.when(nilaiService.updateNilai(eq(1), any(NilaiRequest.class), eq("invalidToken"))).thenThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () -> nilaiController.updateNilai(1, request, "invalidToken"));
    }

    @Test
    @DisplayName("Berhasil Delete nilai ketika Return OK")
    void deleteByIdReturnsOkWhenSuccessful() {
        Mockito.when(nilaiService.deleteNilaiById(1)).thenReturn("Nilai deleted successfully");
        ResponseEntity<String> responseEntity = nilaiController.deleteById(1);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Nilai deleted successfully", responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal Delete nilai ketika Return BadRequest")
    void deleteByIdReturnsBadRequestWhenServiceThrowsException() {
        Mockito.when(nilaiService.deleteNilaiById(1)).thenThrow(IllegalArgumentException.class);
        assertThrows(IllegalArgumentException.class, () -> nilaiController.deleteById(1));
    }
}