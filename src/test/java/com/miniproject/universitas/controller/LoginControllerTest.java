package com.miniproject.universitas.controller;

import com.lowagie.text.DocumentException;
import com.miniproject.universitas.dto.krs.KrsReport;
import com.miniproject.universitas.dto.login.LoginRequest;
import com.miniproject.universitas.dto.login.LoginResponse;
import com.miniproject.universitas.service.KrsService;
import com.miniproject.universitas.service.LoginService;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class LoginControllerTest {

    @InjectMocks
    private LoginController loginController;

    @Mock
    private LoginService loginService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Berhasil login dengan request yang valid")
    void loginWithValidRequest() {
        LoginRequest request = new LoginRequest();
        LoginResponse expectedResponse = new LoginResponse();
        when(loginService.login(request)).thenReturn(expectedResponse);

        ResponseEntity<LoginResponse> responseEntity = loginController.login(request);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal login dengan request yang tidak valid")
    void loginWithInvalidRequest() {
        LoginRequest request = new LoginRequest();
        when(loginService.login(request)).thenThrow(new IllegalArgumentException("Invalid request"));

        assertThrows(IllegalArgumentException.class, () -> loginController.login(request));
    }
}