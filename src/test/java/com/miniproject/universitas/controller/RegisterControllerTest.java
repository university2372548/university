package com.miniproject.universitas.controller;

import com.miniproject.universitas.dto.register.RegisterRequest;
import com.miniproject.universitas.dto.register.RegisterResponse;
import com.miniproject.universitas.repository.UsersRepository;
import com.miniproject.universitas.service.RegisterService;
import com.miniproject.universitas.validation.RegisterValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class RegisterControllerTest {

    @InjectMocks
    RegisterController registerController;

    @Mock
    RegisterService registerService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        registerController = new RegisterController(registerService);
    }

    @Test
    @DisplayName("Berhasil melakukan registrasi")
    void registerUser_ValidRequest_Success() {
        RegisterRequest request = new RegisterRequest();
        request.setEmail("test@test.com");
        request.setAlamat("Test");
        request.setPassword("password");
        request.setRole("admin");

        RegisterResponse expectedResponse = new RegisterResponse();
        expectedResponse.setStatus(HttpStatus.OK.value());
        expectedResponse.setMessage("Register Success");

        when(registerService.registerUser(request)).thenReturn(expectedResponse);

        ResponseEntity<RegisterResponse> responseEntity = registerController.registerUser(request);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("Gagal melakukan registrasi karena email sudah terdaftar")
    void registerUser_ServiceException_ExceptionThrown() {
        RegisterRequest request = new RegisterRequest();
        request.setEmail("test@test.com");
        request.setAlamat("Test");
        request.setPassword("password");
        request.setRole("admin");

        when(registerService.registerUser(request)).thenThrow(new RuntimeException("Service exception"));

        assertThrows(RuntimeException.class, () -> registerController.registerUser(request));
    }
}